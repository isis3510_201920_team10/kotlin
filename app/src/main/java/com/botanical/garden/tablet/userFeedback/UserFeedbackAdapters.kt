package com.botanical.garden.tablet.userFeedback

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData

@BindingAdapter("snackMsg")
fun setSnackMsg(view: View, msg: LiveData<String>?) {
    if(msg==null)
        return

    val msgVal=msg.value
    if(msgVal==null || msgVal=="")
        return

    view.createSnackbarMessage(msgVal)
}

@BindingAdapter("dialogMsg")
fun setDialogMsg(view: View, title:LiveData<String>?,msg:LiveData<String>?) {
    if(msg==null)
        return

    val msgVal=msg.value
    if(msgVal==null || msgVal=="")
        return

    if(title==null)
        return

    val titleVal=title.value
    if(titleVal==null || titleVal=="")
        return

    view.context.createDialogMessage(titleVal,msgVal,null)
}

@BindingAdapter("toastMsg")
fun setToastMsg(view: View, msg:LiveData<String>?) {
    if(msg==null)
        return

    val msgVal=msg.value
    if(msgVal==null || msgVal=="")
        return

    view.context.createToastMessage(msgVal)
}