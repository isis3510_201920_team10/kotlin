package com.botanical.garden.tablet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailModelNew(
    var id: String="",

    val img: String,

    val title:String,

    val type:DetailType,

    val description:String,

    val features: Map<String,String>,

    val latitude: Double,

    val longitude: Double,

    val links:Array<String?>?,

    val startDate: String?,

    val endDate:String?
): Parcelable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DetailModelNew

        if (id != other.id) return false
        if (img != other.img) return false
        if (title != other.title) return false
        if (type != other.type) return false
        if (description != other.description) return false
        if (features != other.features) return false
        if (latitude != other.latitude) return false
        if (longitude != other.longitude) return false
        if (startDate != other.startDate) return false
        if (endDate != other.endDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + img.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + features.hashCode()
        result = 31 * result + latitude.hashCode()
        result = 31 * result + longitude.hashCode()
        result = 31 * result + startDate.hashCode()
        result = 31 * result + endDate.hashCode()
        return result
    }
}