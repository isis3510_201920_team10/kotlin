package com.botanical.garden.tablet.ui.camera

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.FragmentCameraBinding
import com.botanical.garden.tablet.ui.main.CardItemsViewModel
import com.botanical.garden.tablet.userFeedback.createSnackbarMessage
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.fragment_camera.*
import android.view.WindowManager
import android.widget.Toast
import cn.easyar.Engine
import com.botanical.garden.tablet.BuildConfig
import com.botanical.garden.tablet.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_camera.view.*


// Camera permissions request
private const val CAMERA_REQUEST=1


class CameraFragment : Fragment() {

    //----------
    // LOG TAG
    //---------
    val QR_TAG="QR"


    private var detector:BarcodeDetector?=null //BarcodeDetector

    private val key=BuildConfig.EasyARApiKey
    private var glView: GLView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{

        // Create zoneData binding
        val binding: FragmentCameraBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_camera, container, false
        )

        activity?.let {
            binding.viewModel = ViewModelProviders.of(it).get(CardItemsViewModel::class.java)
            // Barcode Detector
            detector = BarcodeDetector.Builder(activity)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build()
        }


        while (detector?.isOperational==false) {
            // Repeat until operational
            Thread.sleep(1000)
            Log.d(QR_TAG,"The detector is not working")
        }

        activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        activity?.let{
            if (!Engine.initialize(it, key)) {
                Log.e("HelloAR", "Initialization Failed.")
                Toast.makeText(it, Engine.errorMessage(), Toast.LENGTH_LONG).show()
                return view
            }
            glView = GLView(it,detector)
        }



        readQR(null)

        binding.lifecycleOwner = viewLifecycleOwner//Set lifecycle owner

        binding.root.btn_detail.setOnClickListener {
            if(activity!=null)
                (activity as MainActivity).goToQRDetail()
        }

        return binding.root
    }

    fun readQR(view:View?){
        requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_REQUEST -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(preview==null)
                {
                    readQR(null)
                    return
                }
                preview.addView(glView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
                //takePicture()
            } else {
                // Handle permission rejected error
                view?.createSnackbarMessage(getString(R.string.error_qr_permission_rejected))
                Log.d(QR_TAG, "Permission Denied!")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        glView?.onResume()
    }

    override fun onPause() {
        glView?.onPause()
        super.onPause()
    }

    override fun onDestroyView() {
        glView=null

        detector?.release()
        detector=null
        super.onDestroyView()
    }

    override fun onDestroy() {
        glView=null

        detector?.release()
        detector=null
        super.onDestroy()
    }
}
