package com.botanical.garden.tablet.ui.search

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.speech.RecognizerIntent

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.FragmentSearchFragmentJBinding
import com.botanical.garden.tablet.model.DetailModelNew

import com.botanical.garden.tablet.repository.DetailRepositoryNew
import com.botanical.garden.tablet.utils.*
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.fragment_search_fragment_j.view.*
import java.util.*
import com.botanical.garden.tablet.ui.main.MainActivity
import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.botanical.garden.tablet.userFeedback.createSnackbarMessage
import kotlinx.android.synthetic.main.fragment_search_fragment_j.*
import kotlinx.coroutines.*
import java.lang.Exception
import java.lang.Runnable
import java.text.Normalizer

private const val VOICE_REQUEST = 100

class SearchFragmentJ : Fragment(), EventAdapter.EventClickListener {

    // Analytics
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    var searchStart=0L
    val delay = 2000L // 2 seconds after user stops typing
    var lastTextEdit = 0L
    var lastText=""
    var handler = Handler()

    //UI
    private var recyclerView: RecyclerView?=null
    private var adapter: ItemAdapter?=null
    private var items: Array<LiveData<DetailModelNew>>?=null
    private var editText: EditText?=null

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentSearchFragmentJBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_search_fragment_j, container, false
        )
        binding.lifecycleOwner=viewLifecycleOwner

        val v = binding.root


        recyclerView = v.events_rv
        editText = v.search_edit_text
        recyclerView?.layoutManager = GridLayoutManager(context, 2)
        adapter = ItemAdapter(context!!, this)
        recyclerView?.adapter = adapter


        GlobalScope.launch{
            setupData()
        }

        v.search_voice_btn.setOnClickListener {
            speak(it)
        }

        //-----------
        // ANALYTICS
        //-----------
        if(context==null)
            return v

        context?.let{
            firebaseAnalytics = FirebaseAnalytics.getInstance(it)
        }

        return v
    }

    private fun speak(v:View?) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale("es", "CO"))
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hola dinos algo")

        try {
            startActivityForResult(intent, VOICE_REQUEST)
        } catch (e: Exception) {
            activity?.createDialogMessage(getString(R.string.voice_error),getString(R.string.voice_error_message), null)
        }

        return
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (VOICE_REQUEST) {
            VOICE_REQUEST -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    if (result==null)
                    {
                        view?.createSnackbarMessage("¡Qué tragedia! Comando no reconocido")
                        return
                    }
                    Log.d("SEARCH",result[0])
                    when {
                        result[0].toUpperCase(Locale("es", "CO")) == "SALIR DEL JARDIN" -> (activity as MainActivity).goToExit()
                        result[0].toUpperCase(Locale("es", "CO")) == "SALIR DEL JARDÍN" -> (activity as MainActivity).goToExit()
                        result[0].toUpperCase(Locale("es", "CO")) == "BUSCAR PLANTA" -> (activity as MainActivity).goToCloseItem()
                        else -> view?.createSnackbarMessage("¡Qué tragedia! Comando no reconocido")
                    }
                }
            }
        }
    }

    suspend fun setupData(){
        withContext(Dispatchers.IO){
            val detailRepository=DetailRepositoryNew()
            items = detailRepository.getAllDetails(context!!)

            adapter?.setInfo(items)

            searchStart=System.currentTimeMillis()
            val inputFinihChecker = Runnable {
                run() {
                    if (System.currentTimeMillis() > (lastTextEdit + delay - 500)) {
                        // ............
                        // ............
                        var bundle = Bundle()
                        bundle.putString(PARAM_NAME, lastText)
                        if(activity!=null)
                            (activity as MainActivity).logLocationEvent(SEARCH_SPECIES,bundle)


                        val endTime=System.currentTimeMillis()
                        bundle = Bundle()
                        bundle.putString(PARAM_START_TIME, searchStart.toString())
                        bundle.putString(PARAM_END_TIME, endTime.toString())
                        bundle.putString(PARAM_TIME, "${endTime-searchStart}")
                        firebaseAnalytics.reportEvent(SEARCH_TIME,bundle)
                    }
                }
            }

            editText?.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    //You need to remove this to run only once
                    handler.removeCallbacks(inputFinihChecker)
                }

                override fun afterTextChanged(editable: Editable) {
                    if(editable.isEmpty())
                    {
                        searchStart=System.currentTimeMillis()
                        tv_instructions1.visibility=View.VISIBLE
                        tv_instructions2.visibility=View.VISIBLE
                        search_voice_btn.visibility=View.VISIBLE
                        events_rv.visibility=View.GONE
                    }

                    if(lastText.isEmpty() && editable.isNotBlank())
                    {
                        tv_instructions1.visibility=View.GONE
                        tv_instructions2.visibility=View.GONE
                        search_voice_btn.visibility=View.GONE
                        events_rv.visibility=View.VISIBLE
                    }

                    lastText=editable.toString()
                    lastTextEdit = System.currentTimeMillis()
                    handler.postDelayed(inputFinihChecker, delay)

                    filter(editable.toString())
                }
            })
        }
    }

    private fun filter(s: String) {
        val results = ArrayList<LiveData<DetailModelNew>>()

        val filter=unaccent(s)
        items?.forEach { item->
            if (unaccent(item.value!!.title).contains(filter)) {
                results.add(item)
            }
        }

        val newItems = arrayOfNulls<LiveData<DetailModelNew>>(results.size)
        for (i in results.indices) {
            newItems[i] = results[i]
        }

        if(results.isEmpty())
        {
            val bundle = Bundle()
            bundle.putString(PARAM_NAME, "Searched miss for species $lastText")
            firebaseAnalytics.reportEvent(SEARCH_MISS,bundle)
        }

        adapter!!.filter(newItems.requireNoNulls())
    }

    private fun unaccent(src: String): String {
		return Normalizer.normalize(src, Normalizer.Form.NFD).replace("[^\\p{ASCII}]".toRegex(), "").toLowerCase()
	}

    override fun onItemClick(v: View, position: Int) {
        val item=adapter?.getItem(position)?.value!!

        // Get detail of search item analytics
        var bundle = Bundle()
        bundle.putString(PARAM_NAME, item.title)
        bundle.putString(PARAM_TYPE, item.type.name)
        firebaseAnalytics.reportEvent(SEARCH_OTHERS,bundle)

        bundle = bundleOf("item" to adapter?.getItem(position)?.value!!)
        findNavController().navigate(R.id.navigation_item_detail,bundle)
    }

    companion object {

        // TODO: Rename and change types and number of parameters
        fun newInstance(): SearchFragmentJ {
            return SearchFragmentJ()
        }
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onDestroyView() {
        recyclerView?.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                // no-op
            }

            override fun onViewDetachedFromWindow(v: View) {
                recyclerView?.adapter = null
                recyclerView=null
                editText=null
                adapter=null
            }
        })

        super.onDestroyView()
    }
    override fun onDestroy() {
        editText = null
        recyclerView?.adapter=null
        recyclerView = null
        adapter=null
        super.onDestroy()
    }


}// Required empty public constructor
