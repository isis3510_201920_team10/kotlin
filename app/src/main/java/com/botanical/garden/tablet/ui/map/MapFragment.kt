package com.botanical.garden.tablet.ui.map

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.FragmentMapBinding
import com.botanical.garden.tablet.ui.main.MapViewModel
import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import androidx.lifecycle.MutableLiveData
import com.botanical.garden.tablet.ui.main.MainActivity


// Debug tag and request codes
private const val TAG="MapFragment"

// Create a LatLngBounds that includes the botanical garden allowed area
private val BOTANICAL_GARDEN:LatLngBounds= LatLngBounds(
    LatLng(4.66593, -74.10414),LatLng(4.67035, -74.09656))

class MapFragment : Fragment() {

    // Get user last location nd if he/she is connected
    private var isConnected=false

    var viewModel: MapViewModel?=null//View Model
    var mapView:MapView?=null
    var destination: LatLng? = null

    // Map
    private var map:GoogleMap?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        isConnected=arguments?.getBoolean("isConnected")!!

        val binding: FragmentMapBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_map, container, false
        )

        // Inflate the layout for this fragment
        val rootView = binding.root
        binding.lifecycleOwner=viewLifecycleOwner

        mapView=rootView.map_view
        mapView?.onCreate(savedInstanceState)

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MapViewModel::class.java) // Get viewmodel from activity
        }




        val endLatLng= viewModel?.endLatLngPoints?.value
        if(endLatLng==null)
        {
            context?.createDialogMessage(getString(R.string.map_permissions_dialog_title),getString(
                            R.string.map_permissions_dialog_content)){
                (activity as MainActivity).backstackFragment(null)
            }
            return rootView
        }

        endLatLng.let {
            if(it.first==null)
            {
                context?.createDialogMessage(getString(R.string.map_permissions_dialog_title),getString(
                    R.string.map_permissions_dialog_content)){
                    (activity as MainActivity).backstackFragment(null)
                }
                return rootView
            }
        }


        val insideGarden=viewModel?.insideGarden?.value
        if(insideGarden==null || !insideGarden)
        {
            val messageId=if(isConnected){ R.string.error_content_user_not_in_garden_connected} else {R.string.error_content_user_not_in_garden_not_connected}
            this.activity?.let {
                it.createDialogMessage(
                    getString(R.string.error_title_user_not_in_garden),
                    getString(messageId)
                ) { if(!isConnected)  (it as MainActivity).backstackFragment(null)}

            }

            if(!isConnected)
                return rootView // Early return in order to not create the map
        }

        mapView?.getMapAsync { mMap ->
            map=mMap
            setUpMap(isConnected)
        }

        binding.viewModel=viewModel//Get viewmodel reference for the future

        return rootView
    }

    private fun setUpMap(isConnected:Boolean) {

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(activity)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }


        map?.mapType = if (!isConnected) GoogleMap.MAP_TYPE_NONE else GoogleMap.MAP_TYPE_NORMAL

        Log.d(TAG, "Connected $isConnected")

        if (!isConnected) {
            map?.setMinZoomPreference(16.0f)
            map?.setMaxZoomPreference(17.0f)
            map?.addTileOverlay((TileOverlayOptions().tileProvider(OfflineTileProvider(resources.assets))))
            map?.setLatLngBoundsForCameraTarget(BOTANICAL_GARDEN)
            Log.d(TAG, "Overlay successful")
        } else {
            map?.setMinZoomPreference(15.0f)
            map?.setMaxZoomPreference(20.0f)
        }

        // 1
        map?.isMyLocationEnabled = true
        if(viewModel?.insideGarden?.value==true)
            map?.moveCamera(CameraUpdateFactory.newLatLngBounds(BOTANICAL_GARDEN, 17))

        val location=viewModel?.endLatLngPoints?.value?.first
        location?.let{
            val currentLatLng = LatLng(
                it.latitude,
                it.longitude
            )
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
        }

        // If the person is not inside the garden, show a route to it
        if(isConnected && viewModel?.insideGarden?.value==false)
        {
            viewModel?.setEnd(BOTANICAL_GARDEN.center)
        }
    }

    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onDestroyView() {
        map?.isMyLocationEnabled=false
        map?.clear()

        mapView?.onDestroy()
        mapView=null

        viewModel?.setEnd(null)
        viewModel=null

        super.onDestroyView()
    }

    override fun onDestroy() {
        map?.isMyLocationEnabled=false
        map?.clear()

        mapView?.onDestroy()
        mapView=null

        viewModel?.setEnd(null)
        viewModel=null

        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }
}
