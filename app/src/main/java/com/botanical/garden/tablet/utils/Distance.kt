package com.botanical.garden.tablet.utils

import android.location.Location
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlin.math.abs

var locationA=Location("point A")
var locationB=Location("point B")

fun similarDistance(pointA:Double,pointB:Double):Boolean{
    Log.d("resPOINT","Distance ${abs(pointA-pointB)}")
    Log.d("resPOINT","Distance ${pointA} $pointB")
    return abs(pointA-pointB) <0.000025
}

fun getDistance(start: LatLng, end: LatLng):Float{ // Adapted from https://stackoverflow.com/questions/14394366/find-distance-between-two-points-on-map-using-google-map-api-v2/15351351#15351351
    locationA.latitude = start.latitude
    locationA.longitude = start.longitude

    locationB.latitude = end.latitude
    locationB.longitude = end.longitude

    return locationA.distanceTo(locationB) / 1000 // Get distance in google "meter" units. It´s still under debate the real units of this, but it is consistent after testing
}

fun isInsideArea(latitude:Double,longitude:Double,area:LatLngBounds):Boolean {
    val northeast = area.northeast
    val southwest = area.southwest

    val eastBound = longitude < northeast.longitude
    val westBound = longitude > southwest.longitude

    val inLong = if (northeast.longitude < southwest.longitude) {
        eastBound || westBound
    } else {
        eastBound && westBound
    }

    val inLat = latitude > southwest.latitude && latitude < northeast.latitude
    return inLat && inLong
}

fun test(latitude:Double,longitude:Double,path:Array<LatLng>):Boolean {
    var crossings=0
    // for each edge
    for ( i in 0 until path.size) {
        val a = path[i]
        var j = i + 1;
        if (j >= path.size) {
            j = 0;
        }
        val b = path[j]
        if (rayCrossesSegment(latitude,longitude, a, b)) {
            crossings++;
        }
    }

    // odd number of crossings?
    return (crossings % 2 == 1)
}

fun rayCrossesSegment(latitude: Double,longitude: Double,a:LatLng,b:LatLng):Boolean{
        var px = longitude
        var py = latitude
        var ax = a.longitude
        var ay = a.latitude
        var bx = b.longitude
        var by = b.latitude
        if (ay > by) {
            ax = b.longitude
            ay = b.latitude
            bx = a.longitude
            by = a.latitude
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0) {
            px += 360;
        }
        if (ax < 0) {
            ax += 360;
        }
        if (bx < 0) {
            bx += 360;
        }

        if (py == ay || py == by) py += 0.00000001;
        if ((py > by || py < ay) || (px > Math.max(ax, bx))) return false;
        if (px < Math.min(ax, bx)) return true;

        var red = if (ax != bx) ((by - ay) / (bx - ax)) else Double.MAX_VALUE
        var blue = if (ax != px)  ((py - ay) / (px - ax)) else Double.MAX_VALUE
        return (blue >= red)
}