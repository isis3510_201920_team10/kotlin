package com.botanical.garden.tablet.model

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceModelNew(
    override var id: String="",

    override val name:String,

    override val imageUrl: String,

    override val description: String,

    override val type:DetailType=DetailType.PLACE,

    override val features: Map<String,String>,

    override val latitude: Double,

    override val longitude: Double
): Parcelable,ItemModel(){
    override fun toDetailModel(): LiveData<DetailModelNew> {
        val ans= MutableLiveData<DetailModelNew>()

        val detail=DetailModelNew(
            id,
            imageUrl,
            name,
            type,
            description,
            features,
            latitude,
            longitude,
            null,
            null,
            null
        )

        ans.postValue(detail)
        return ans
    }
}

