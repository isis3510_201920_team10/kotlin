package com.botanical.garden.tablet.repository

import android.content.Context
import com.botanical.garden.tablet.model.DetailType
import com.botanical.garden.tablet.model.PlantModelNew
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject

class PlantRepositoryNew {

    suspend fun getAllPlants(context: Context): Array<PlantModelNew>
    {
        val allPlants: MutableList<PlantModelNew> = mutableListOf<PlantModelNew>()
        val jsonString: String = context.assets.open("data/zones.json").bufferedReader().use {it.readText()}
        val jsonArray: JSONArray = JSONArray(jsonString)


        return withContext(Dispatchers.IO) {

            for(j in 0 until jsonArray.length()){

                val chosenZone: JSONArray = jsonArray.getJSONObject(j).getJSONArray("plants")

                for(i in 0 until chosenZone.length()){

                    val features:MutableMap<String,String> = mutableMapOf()
                    val currentPlant: JSONObject = chosenZone.getJSONObject(i)

                    val jsonFeatures = currentPlant.getJSONObject("features")

                    features["Planta por tipo de duracion"] = jsonFeatures.getString("Planta por tipo de duración")
                    features["Especificaciones"] = jsonFeatures.getString("Especificaciones")
                    features["Crecimiento"] = jsonFeatures.getString("Crecimiento")
                    features["Usos varios"] = jsonFeatures.getString("Usos varios")

                    val id: String = currentPlant.getString("id")
                    val name: String = currentPlant.getString("name")
                    val descripcion = currentPlant.getString("description")
                    val scientificName = currentPlant.getString("scientificName")
                    val imageUrl = currentPlant.getString("imageUrl")
                    val lat = currentPlant.getDouble("latitude")
                    val long = currentPlant.getDouble("longitude")
                    val tempLinks = currentPlant.getJSONArray("links")

                    val links= arrayOfNulls<String>(tempLinks.length())
                    for(k in 0 until tempLinks.length()){
                        links[k]=tempLinks.getString(k)
                    }

                    val plant = PlantModelNew(id,name,imageUrl,scientificName,descripcion,
                        DetailType.PLANT,features,lat,long,links)
                    allPlants.add(plant)

                }


            }
            allPlants.toTypedArray()
        }
    }

    fun getPlantById(context: Context, id: Int): PlantModelNew?{
        val jsonString: String = context.assets.open("data/zones.json").bufferedReader().use {it.readText()}
        val jsonArray: JSONArray = JSONArray(jsonString)

        for(j in 0 until jsonArray.length()){

            val chosenZone: JSONArray = jsonArray.getJSONObject(j).getJSONArray("plants")

            for(i in 0 until chosenZone.length()){

                val features:MutableMap<String,String> = mutableMapOf()
                val currentPlant: JSONObject = chosenZone.getJSONObject(i)

                if(currentPlant.getInt("id") == (id)){

                    val jsonFeatures = currentPlant.getJSONObject("features")

                    features["Planta por tipo de duracion"] = jsonFeatures.getString("Planta por tipo de duración")
                    features["Especificaciones"] = jsonFeatures.getString("Especificaciones")
                    features["Crecimiento"] = jsonFeatures.getString("Crecimiento")
                    features["Usos varios"] = jsonFeatures.getString("Usos varios")

                    val id: String = currentPlant.getString("id")
                    val name: String = currentPlant.getString("name")
                    val descripcion = currentPlant.getString("description")
                    val scientificName = currentPlant.getString("scientificName")
                    val imageUrl = currentPlant.getString("imageUrl")
                    val lat = currentPlant.getDouble("latitude")
                    val long = currentPlant.getDouble("longitude")
                    val tempLinks = currentPlant.getJSONArray("links")


                    val links= arrayOfNulls<String>(tempLinks.length())
                    for(k in 0 until tempLinks.length()){
                        links[k]=tempLinks.getString(k)
                    }

                    val plant = PlantModelNew(id,name,imageUrl,scientificName,descripcion,
                        DetailType.PLANT,features,lat,long,links)
                    return plant
                }

            }

        }

        return null

    }
}