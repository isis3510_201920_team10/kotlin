package com.botanical.garden.tablet.ui.detail

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.size

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.DetailModelNew
import com.botanical.garden.tablet.model.DetailType
import com.botanical.garden.tablet.model.EventModelNew
import com.botanical.garden.tablet.ui.main.MainActivity
import com.botanical.garden.tablet.utils.*
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.fragment_event_detail.*
import kotlinx.android.synthetic.main.fragment_event_detail.post_article_btn


class ItemDetailFragment : Fragment() {

    private var firebaseAnalytics:FirebaseAnalytics?=null

    private var item: DetailModelNew? = null

    private var itemName: TextView? = null
    private var itemDescription: TextView? = null
    private var itemImg: ImageView? = null
    private var allContainer: LinearLayout? = null

    private var btnAddress: Button? = null

    init {
        item = null
        // Required empty public constructor
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        item=arguments?.getParcelable<DetailModelNew>("item")
        return inflater.inflate(R.layout.fragment_event_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setupContent()

        // Firebase analytics event
        if(context==null)
            return
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        if(item?.type==DetailType.PLANT){
            item?.let{
                val bundle = Bundle()
                bundle.putString(PARAM_NAME, it.title)
                it.features["Usos varios"].let{n->
                    bundle.putString("feature1",n)
                }
                it.features["Planta por tipo de duracion"].let{n->
                    bundle.putString("feature2",n)
                }
                firebaseAnalytics?.reportEvent(DETAIL_SPECIES_EVENT,bundle)

            }
        }

        float_btn_detail!!.setOnClickListener { view->
            item?.let{
                var bundle = Bundle()
                bundle.putString(PARAM_NAME, it.title)
                bundle.putString(PARAM_LAT, it.latitude.toString())
                bundle.putString(PARAM_LON, it.longitude.toString())

                bundle.putString(PARAM_START_DATE, it.startDate)
                bundle.putString(PARAM_END_DATE, it.endDate)

                firebaseAnalytics?.reportEvent(ATTRACTED_EVENT,bundle)

                if(item?.type==DetailType.EVENT){
                    bundle = Bundle()
                    bundle.putString(PARAM_NAME, it.title)
                    bundle.putString(PARAM_LAT, it.latitude.toString())
                    bundle.putString(PARAM_LON, it.longitude.toString())

                    bundle.putString(PARAM_START_DATE, it.startDate)
                    bundle.putString(PARAM_END_DATE, it.endDate)

                    firebaseAnalytics?.reportEvent(ATTRACTED_TO_EVENT,bundle)
                }

                (activity as MainActivity).goToMap(view, it.latitude, it.longitude)
            }

        }

        if(item?.type==DetailType.PLANT)
        {
            research_title.visibility=View.VISIBLE
            post_article_btn.visibility=View.VISIBLE
            post_article_btn.setOnClickListener {v->
                (activity as MainActivity).postArticle(v,item!!.title)
            }

            setupLinks()
        }

    }

    private fun setupContent(){
        itemName = event_name
        itemDescription = event_description
        itemImg =event_image
        allContainer = all_container



        itemName!!.text = item!!.title
        itemDescription!!.text = item!!.description
        Glide.with(this).load(item!!.img).into(itemImg!!)

        val headerParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        headerParams.setMargins(0, 20, 0, 0)


        val contentParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        contentParams.setMargins(0, 10, 0, 0)

        var header:TextView
        var content:TextView

        for ((key, value) in item!!.features) {
            header = TextView(context)
            header.text = key
            header.textSize = 25f
            header.setTextColor(resources.getColor(R.color.textColor))
            header.layoutParams = headerParams


            content = TextView(context)
            content.text = value
            content.textSize = 20f
            content.setTextColor(resources.getColor(R.color.textColor))
            content.layoutParams = contentParams

            allContainer?.let{
                it.addView(header,it.size-2)
                it.addView(content,it.size-2)
            }
        }
    }

    private fun setupLinks(){
        var textView:TextView?
        val contentParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        contentParams.setMargins(10, 10, 0, 0)

        item!!.links?.forEachIndexed { index,txt->
            textView = TextView(context)
            textView?.textSize = 20f
            textView?.setTextColor(resources.getColor(R.color.linkColor))
            textView?.movementMethod= LinkMovementMethod.getInstance()
            textView?.layoutParams = contentParams

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView?.text = Html.fromHtml("${index+1}. <a href='$txt'>$txt</a>", Html.FROM_HTML_MODE_COMPACT)
            }
            else {
                textView?.text = Html.fromHtml("${index+1}. <a href='$txt'>$txt</a>")
            }

            textView?.setOnClickListener{ v->
                val bundle = Bundle()
                bundle.putString(PARAM_NAME, item!!.title)
                bundle.putString(PARAM_LINK,txt)
                firebaseAnalytics?.reportEvent(OPEN_RESEARCH_EVENT,bundle)
            }

            allContainer?.let{
                it.addView(textView,it.size-1)
            }
        }

        if(item!!.links.isNullOrEmpty()){
            textView = TextView(context)
            textView?.textSize = 20f
            textView?.setTextColor(resources.getColor(R.color.linkColor))
            textView?.text=getString(R.string.no_research_for_plant)
            allContainer?.let{
                it.addView(textView,it.size-1)
            }
        }
    }

    override fun onDestroyView() {
        item = null
        itemName = null
        itemDescription = null
        itemImg = null
        allContainer = null
        btnAddress = null
        firebaseAnalytics=null

        super.onDestroyView()
    }

    override fun onDestroy() {
        item = null
        itemName = null
        itemDescription = null
        itemImg = null
        allContainer = null
        btnAddress = null
        firebaseAnalytics=null

        super.onDestroy()
    }
}


