package com.botanical.garden.tablet.ui.main

import android.app.Application
import android.util.Log
import androidx.core.os.bundleOf
import androidx.lifecycle.*
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.PlantModelNew
import com.botanical.garden.tablet.model.Status
import com.botanical.garden.tablet.model.ZoneSimpleModel
import com.botanical.garden.tablet.repository.RouteRepository
import com.botanical.garden.tablet.repository.ZoneRepository
import com.botanical.garden.tablet.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

private const val CLOSE_RADIO=0.1f // Meters from where an item is considered close
private const val RECALCULATE_DIFF=0.1f // MAx meters difference from a given point
const val REPLACE_DIFF=0.04f // MAx meters difference from a given point to assume it

private const val TAG="MapVM" // Debug tag
private val exits=arrayOf(LatLng(4.66801, -74.09669), LatLng(4.6684, -74.10314)) // Burnt exits

class MapViewModel (application: Application) : AndroidViewModel(application) {

    // Firebase Analytics
    private val firebaseAnalytics=FirebaseAnalytics.getInstance(application)
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)


    private val routeRepository=RouteRepository()
    private var zoneRepository= ZoneRepository(application.resources.assets)


    private val _status=MutableLiveData<Status>(Status.SUCCESS)
    private val _msg=MutableLiveData<String>()

    private var _isInsideGarden= MutableLiveData<Boolean>()
    private var _endLatLngPoints=MutableLiveData<Pair<LatLng?,LatLng?>>()
    private var _route= MutableLiveData<List<LatLng>?>()

    val endLatLngPoints: LiveData<Pair<LatLng?,LatLng?>>
        get() = _endLatLngPoints

    val insideGarden: LiveData<Boolean>
        get() = _isInsideGarden


    val status: LiveData<Status>
        get() = _status
    val msg: LiveData<String>
        get() = _msg


    val route: LiveData<List<LatLng>?> = Transformations
        .switchMap(_endLatLngPoints) { points ->
            val start=points.first
            val end=points.second
            if (start==null || end==null) {
                Log.d(TAG,"No values to start")
                _route
            }
            else {
                // Other thread launch
                val ans:LiveData<List<LatLng>?> =liveData{
                    if(recalculateRoute(_route.value))
                    {
                        Log.d(TAG,"Recalculate route")
                        val resource=routeRepository.getRoute(start,end)
                        _status.value=resource.status
                        _msg.value=resource.message
                        emit(resource.data)
                    }
                }
                ans
            }
        }

    val currentZone: LiveData<ZoneSimpleModel> = Transformations
        .switchMap(_endLatLngPoints) { points->
            val start=points.first
            val ans:LiveData<ZoneSimpleModel> = if(start==null){
                MutableLiveData()
            } else {
                liveData(uiScope.coroutineContext + Dispatchers.IO){
                    Log.d(TAG,"Received close zones")
                    emit(zoneRepository.getZoneCloseToPosition(start))
                }
            }
            ans
        }

    val closeItems: LiveData<List<PlantModelNew>> = Transformations
        .switchMap(_endLatLngPoints) { points->
            val start=points.first
            if(start==null){
                MutableLiveData()
            } else {
                liveData(uiScope.coroutineContext + Dispatchers.IO){
                    emit(zoneRepository.getItemsCloseToPosition(start,CLOSE_RADIO))
                }

            }
        }

    fun setInsideGarden(inside:Boolean)
    {
        _isInsideGarden.value=inside
    }

    fun setStart(start:LatLng)
    {
        val pair=_endLatLngPoints.value
        this._endLatLngPoints.value=Pair(start,pair?.second)
    }

    fun setEnd(end:LatLng?)
    {
        val pair=_endLatLngPoints.value
        this._endLatLngPoints.value=Pair(pair?.first,end)
        this._route.value=listOf()
    }

    fun goToExit(){
        Log.d(TAG,"Start ${ _endLatLngPoints.value?.first}")
        val start= _endLatLngPoints.value?.first ?: return

        var closestExit=exits[0]
        var distance= getDistance(start,closestExit)
        exits.forEach {
            val tempDistance=getDistance(start,it)
            if (tempDistance<distance){
                closestExit=it
                distance=tempDistance
            }
        }
        _route.value=null

        setEnd(closestExit)
    }

    /**
     * Decide if route needs to be recalculated. If conditions are met, modify the route points as well (such as being too close to a given point or reaching the goal)
     */
    private fun recalculateRoute(points:List<LatLng>?):Boolean{
        if(points==null || points.size<2)
            return true

        val end =_endLatLngPoints.value?.second
        val lastPoint=points[points.size-1]

        if(end!=null && end.latitude!=lastPoint.latitude && end.longitude!=lastPoint.longitude) // If the end has changed, recalculate routes
            return true

        val start= _endLatLngPoints.value?.first ?: return false
        val firstPoint=points[1]
        var copy=points.toMutableList()
        val distance=getDistance(start,firstPoint)

        Log.d(TAG,"Distance $distance: $start VS $firstPoint")

        if(distance > RECALCULATE_DIFF){ // The user is farther from the accepted difference
            val bundle = bundleOf(
                PARAM_LAT to start.latitude, PARAM_LON to start.longitude,
                PARAM_TARGET_LAT to end?.latitude, PARAM_TARGET_LON to end?.longitude)
            firebaseAnalytics.reportEvent(RECALCULATE_ROUTE_EVENT,bundle)
            return true
        }

        if(getDistance(start,points[points.size-1])< REPLACE_DIFF) { // The user reached the goal
            copy= mutableListOf()
            _status.value=Status.ARRIVED
            _msg.value=getApplication<Application>().getString(R.string.map_arrived_to_destiny)
        }
        else
        {
            if(points.size>2 && distance<= REPLACE_DIFF) { // The user is too close to the next point, hence he arrived there
                copy.removeAt(1)
            }
            copy[0]=start // Set the new route´s first point to the user´s current position
        }

        Log.d(TAG,"Copy $copy")
        _route.value=copy

        return false
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}