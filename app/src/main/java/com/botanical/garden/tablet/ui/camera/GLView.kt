//================================================================================================================================
//
// Copyright (c) 2015-2019 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

package com.botanical.garden.tablet.ui.camera

import android.annotation.SuppressLint
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.egl.EGLDisplay
import javax.microedition.khronos.egl.EGLSurface
import javax.microedition.khronos.opengles.GL10

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.opengl.GLSurfaceView
import android.util.Log
import android.view.Surface
import android.view.WindowManager

import cn.easyar.Engine
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.ui.main.MainActivity
import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector

private const val TAG="ARCore"

@SuppressLint("ViewConstructor")
class GLView(context: Context, private val detector: BarcodeDetector?) : GLSurfaceView(context) {
    private val lock = Any()
    private var finishing = false

    private var initialized = false
    private var width_ = 1
    private var height_ = 1

    private var helloAR: HelloAR? = null
    private var scanned=false
    private var ans:Bitmap?=null

    private val activity: Activity?
        get() {
            var context = context
            while (context is ContextWrapper) {
                if (context is Activity) {
                    return context
                }
                context = context.baseContext
            }
            return null
        }

    init {
        //setPreserveEGLContextOnPause(true) //uncomment if EGL context need to be preserved on pause
        setEGLContextFactory(ContextFactory())
        setEGLWindowSurfaceFactory(WindowSurfaceFactory())
        setEGLConfigChooser(ConfigChooser())

        this.setRenderer(object : GLSurfaceView.Renderer {
            override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
                if (!initialized) {
                    initialized = true
                    helloAR = HelloAR()
                    helloAR!!.initialize()
                } else {
                    helloAR!!.recreate_context()
                }
                helloAR!!.start()

                Log.d("XXXX","OnSurfaceCreated")
            }

            override fun onSurfaceChanged(gl: GL10, w: Int, h: Int) {
                width_ = w
                height_ = h
                Log.d("XXXX","OnSurfaceChanged")

            }

            override fun onDrawFrame(gl: GL10) {
                if (!initialized) {
                    return
                }

                // TODO: BG Thread
                ans= helloAR!!.render(width_, height_, GetScreenRotation())
                Log.d(TAG,"Scanned? $scanned")
                if(ans!=null && !scanned) {
                    scanQR(ans)
                }
                else
                {
                    (activity as MainActivity).setDetailId(null)
                }

            }
        })
        this.setZOrderMediaOverlay(true)
    }

    fun scanQR(bitmap: Bitmap?){
        // Get result
        try {
            if (detector?.isOperational==true && bitmap != null) {
                // Read zoneData from detector
                Log.d(TAG,"${bitmap.width}")
                val frame = Frame.Builder().setBitmap(bitmap).build()
                val barcodes = detector.detect(frame)
                for (index in 0 until barcodes.size()) {
                    // See all codes read
                    val code = barcodes.valueAt(index)
                    val type = barcodes.valueAt(index).valueFormat
                    when (type) {
                        Barcode.CONTACT_INFO -> Log.i(TAG,code.contactInfo.title)
                        Barcode.EMAIL -> Log.i(TAG, code.email.address)
                        Barcode.ISBN -> Log.i(TAG, code.rawValue)
                        Barcode.PHONE -> Log.i(TAG, code.phone.number)
                        Barcode.PRODUCT -> Log.i(TAG, code.rawValue)
                        Barcode.SMS -> Log.i(TAG, code.sms.message)
                        Barcode.TEXT -> {
                            Log.d(TAG,"Code ${code.rawValue}")

                            (activity as MainActivity).setDetailId(code.rawValue.toInt())
                        }

                        Barcode.URL -> Log.i(TAG, "url: " + code.url.url)
                        Barcode.WIFI -> Log.i(TAG, code.wifi.ssid)
                        Barcode.GEO -> Log.i(TAG, code.geoPoint.lat.toString() + ":" + code.geoPoint.lng)
                        Barcode.CALENDAR_EVENT -> Log.i(TAG,code.calendarEvent.description)
                        Barcode.DRIVER_LICENSE ->
                            Log.i(TAG, code.driverLicense.licenseNumber)

                        else -> Log.i(TAG, code.rawValue)
                    }
                }
                if (barcodes.size() == 0) {
                    activity?.let{
                        it.createDialogMessage(it.getString(R.string.error_empty_data),it.getString(R.string.error_empty_qr_info),null)
                    }
                    Log.d(TAG, "The scanned QR is empty!")
                }
            } else {
                activity?.let{
                    it.createDialogMessage(it.getString(R.string.error_empty_data),it.getString(R.string.error_scan_not_setup),null)
                }
                Log.d(TAG, "Could not set up the Barcode detector!")
            }
        } catch (e: Exception) {
            activity?.let{
                it.createDialogMessage(it.getString(R.string.error_empty_data),it.getString(R.string.error_unreadable_qr),null)
            }
            Log.e(TAG, "Error loading image to google vision API \n $e")
        }
    }

    private fun onSurfaceDestroyed() {
        val b: Boolean
        synchronized(lock) {
            b = finishing
        }
        if (initialized && b) {
            initialized = false
            helloAR!!.stop()
            helloAR!!.dispose()
            helloAR = null
        }
    }

    override fun onResume() {
        super.onResume()
        Engine.onResume()
        scanned=false
    }

    override fun onPause() {
        val a = activity
        if (a != null) {
            val b = a.isFinishing
            synchronized(lock) {
                finishing = b
            }
        }
        Engine.onPause()
        super.onPause()
    }

    override fun onDetachedFromWindow() {
        val a = activity
        if (a != null) {
            val b = a.isFinishing
            synchronized(lock) {
                finishing = b
            }
        }
        Engine.onPause()
        super.onPause()
        super.onDetachedFromWindow()
    }

    private fun GetScreenRotation(): Int {
        val rotation = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation
        val orientation: Int
        when (rotation) {
            Surface.ROTATION_0 -> orientation = 0
            Surface.ROTATION_90 -> orientation = 90
            Surface.ROTATION_180 -> orientation = 180
            Surface.ROTATION_270 -> orientation = 270
            else -> orientation = 0
        }
        return orientation
    }

    private class ContextFactory : GLSurfaceView.EGLContextFactory {

        override fun createContext(egl: EGL10, display: EGLDisplay, eglConfig: EGLConfig): EGLContext {
            val context: EGLContext
            val attrib = intArrayOf(EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE)
            context = egl.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrib)
            return context
        }

        override fun destroyContext(egl: EGL10, display: EGLDisplay, context: EGLContext) {
            egl.eglDestroyContext(display, context)
        }

        companion object {
            private val EGL_CONTEXT_CLIENT_VERSION = 0x3098
        }
    }

    private inner class WindowSurfaceFactory : GLSurfaceView.EGLWindowSurfaceFactory {
        override fun createWindowSurface(egl: EGL10, display: EGLDisplay, config: EGLConfig, nativeWindow: Any): EGLSurface? {
            var result: EGLSurface? = null
            try {
                result = egl.eglCreateWindowSurface(display, config, nativeWindow, null)
            } catch (e: IllegalArgumentException) {
                // This exception indicates that the surface flinger surface
                // is not valid. This can happen if the surface flinger surface has
                // been torn down, but the application has not yet been
                // notified via SurfaceHolder.Callback.surfaceDestroyed.
                // In theory the application should be notified first,
                // but in practice sometimes it is not. See b/4588890
                Log.e("GLSurfaceView", "eglCreateWindowSurface", e)
            }

            return result
        }

        override fun destroySurface(egl: EGL10, display: EGLDisplay, surface: EGLSurface) {
            onSurfaceDestroyed()
            egl.eglDestroySurface(display, surface)
        }
    }

    private class ConfigChooser : GLSurfaceView.EGLConfigChooser {
        override fun chooseConfig(egl: EGL10, display: EGLDisplay): EGLConfig {
            val EGL_OPENGL_ES2_BIT = 0x0004
            val attrib = intArrayOf(EGL10.EGL_RED_SIZE, 4, EGL10.EGL_GREEN_SIZE, 4, EGL10.EGL_BLUE_SIZE, 4, EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, EGL10.EGL_NONE)

            val num_config = IntArray(1)
            egl.eglChooseConfig(display, attrib, null, 0, num_config)

            val numConfigs = num_config[0]
            if (numConfigs <= 0)
                throw IllegalArgumentException("fail to choose EGL configs")

            val configs = arrayOfNulls<EGLConfig>(numConfigs)
            egl.eglChooseConfig(display, attrib, configs, numConfigs, num_config)

            for (config in configs) {
                val `val` = IntArray(1)
                var r = 0
                var g = 0
                var b = 0
                var a = 0
                var d = 0
                if (egl.eglGetConfigAttrib(display, config, EGL10.EGL_DEPTH_SIZE, `val`))
                    d = `val`[0]
                if (d < 16)
                    continue

                if (egl.eglGetConfigAttrib(display, config, EGL10.EGL_RED_SIZE, `val`))
                    r = `val`[0]
                if (egl.eglGetConfigAttrib(display, config, EGL10.EGL_GREEN_SIZE, `val`))
                    g = `val`[0]
                if (egl.eglGetConfigAttrib(display, config, EGL10.EGL_BLUE_SIZE, `val`))
                    b = `val`[0]
                if (egl.eglGetConfigAttrib(display, config, EGL10.EGL_ALPHA_SIZE, `val`))
                    a = `val`[0]
                if (r == 8 && g == 8 && b == 8 && a == 0)
                    return config!!
            }

            return configs[0]!!
        }
    }
}
