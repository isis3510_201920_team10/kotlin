package com.botanical.garden.tablet.ui.detail


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import androidx.fragment.app.Fragment

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.size

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.PlantModelNew
import com.botanical.garden.tablet.ui.main.MainActivity
import com.botanical.garden.tablet.utils.*
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.fragment_plant_detail.*


/**
 * A simple [Fragment] subclass.
 * Use the [PlantDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlantDetailFragment : Fragment() {

    private var firebaseAnalytics:FirebaseAnalytics?=null

    private var plant: PlantModelNew? = null
    private var btnVoice: Button? = null

    private var plantImg: ImageView? = null
    private var name: TextView? = null
    private var descripcion: TextView? = null
    private var especificaciones: TextView? = null
    private var usos: TextView? = null
    private var duracion: TextView? = null
    private var crecimiento: TextView? = null
    private var allContainer: LinearLayout? = null

    private var backBtn: ImageButton? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        plant=arguments?.getParcelable<PlantModelNew>("plant")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_plant_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        name = plant_name
        descripcion = plant_description
        especificaciones = especificaciones_text
        usos = usos_text
        duracion = plant_duracion_text
        crecimiento = crecimiento_text
        plantImg = plant_img
        backBtn = back_btn
        allContainer = all_container

        setupContent()
        setupLinks()

        // FB ANALYTICS SETUP
        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        plant?.let{ it ->
            val bundle = Bundle()
            bundle.putString(PARAM_NAME, it.name)
            it.features["Usos varios"].let{n->
                bundle.putString("feature1",n)
            }
            firebaseAnalytics?.reportEvent(DETAIL_SPECIES_EVENT,bundle)
        }


        float_btn!!.setOnClickListener { v -> (activity as MainActivity).goToMap(v, plant!!.latitude, plant!!.longitude) }
        post_article_btn.setOnClickListener { (activity as MainActivity).postArticle(it,plant!!.name) }
    }

    private fun setupContent(){
        name?.text = plant!!.name
        Log.v(javaClass.toString(), plant!!.description)
        descripcion?.text = plant!!.description
        especificaciones?.text = plant!!.features["Especificaciones"]
        usos?.text = plant!!.features["Usos varios"]

        duracion?.text = plant!!.features["Planta por tipo de duracion"]
        crecimiento?.text = plant!!.features["Crecimiento"]
        Glide.with(this).load(plant!!.imageUrl).centerCrop().into(plantImg!!)
    }

    private fun setupLinks(){
        var textView:TextView?
        val contentParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        contentParams.setMargins(10, 10, 0, 0)

        plant!!.links.forEachIndexed {index,txt->
            textView = TextView(context)
            textView?.textSize = 20f
            textView?.setTextColor(resources.getColor(R.color.linkColor))
            textView?.movementMethod=LinkMovementMethod.getInstance()
            textView?.isClickable=true

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView?.text = Html.fromHtml("${index+1}. <a href='$txt'>$txt</a>", Html.FROM_HTML_MODE_COMPACT)
            }
            else {
                textView?.text = Html.fromHtml("${index+1}. <a href='$txt'>$txt</a>")
            }


            textView?.layoutParams = contentParams
            textView?.setOnClickListener{ v->
                val bundle = Bundle()
                bundle.putString(PARAM_NAME, plant!!.name)
                bundle.putString(PARAM_LINK,txt)
                firebaseAnalytics?.reportEvent(OPEN_RESEARCH_EVENT,bundle)
            }

            allContainer?.let{
                it.addView(textView,it.size-1)
            }
        }

        if(plant!!.links.isNullOrEmpty()){
            textView = TextView(context)
            textView?.textSize = 20f
            textView?.setTextColor(resources.getColor(R.color.linkColor))
            textView?.text=getString(R.string.no_research_for_plant)
            allContainer?.let{
                it.addView(textView,it.size-1)
            }
        }
    }

    override fun onDestroyView() {
        plant = null
        btnVoice = null
        plantImg = null
        name = null
        descripcion = null
        especificaciones = null
        usos = null
        duracion= null
        crecimiento = null
        backBtn = null

        super.onDestroyView()
    }

    override fun onDestroy() {
        plant = null
        btnVoice = null
        plantImg = null
        name = null
        descripcion = null
        especificaciones = null
        usos = null
        duracion= null
        crecimiento = null
        backBtn = null
        allContainer = null

        super.onDestroy()
    }

    companion object {
        fun newInstance(): PlantDetailFragment {
            return PlantDetailFragment()
        }
    }
}// Required empty public constructor
