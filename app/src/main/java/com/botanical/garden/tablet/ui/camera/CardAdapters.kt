package com.botanical.garden.tablet.ui.camera

import android.view.View
import android.widget.Button
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData

@BindingAdapter("msgVisibility")
fun setButton(btn: Button, idData: LiveData<Int?>)
{
    val id=idData.value
    if(id!==null)
        btn.visibility= View.VISIBLE
    else
        btn.visibility= View.INVISIBLE
}