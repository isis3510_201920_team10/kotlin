package com.botanical.garden.tablet.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

abstract class ItemModel {
    abstract var id: String

    abstract val name: String

    abstract val type:DetailType

    abstract val imageUrl: String

    abstract val description: String

    abstract val features: Map<String, String>

    abstract val latitude: Double

    abstract val longitude: Double

    override fun toString():String{
        return "$id:$name"
    }

    abstract fun toDetailModel(): LiveData<DetailModelNew>
}

