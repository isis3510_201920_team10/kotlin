package com.botanical.garden.tablet.utils

import android.os.Bundle
import android.util.Log
import com.google.firebase.analytics.FirebaseAnalytics

private const val FIREBASE_ANALYTICS_TAG="FirebaseAnalytics"

// Custom events
const val SEARCH_SPECIES="search_species"
const val SEARCH_OTHERS="search_others"
const val SEARCH_TIME="search_time"
const val SEARCH_MISS="search_miss"
const val ATTRACTED_EVENT="attracted_event"
const val ATTRACTED_TO_EVENT="attracted_to_event"
const val SCAN_QR_EVENT="scan_qr"
const val RECALCULATE_ROUTE_EVENT="recalculate_route"
const val CHANGE_LOCATION_EVENT="change_location"
const val EXIT_GARDEN_EVENT="exit_garden"
const val WIFI_QUESTIONNAIRE_EVENT="wifi_questionnaire"
const val USER_LOCATION_EVENT="user_location"
const val UPLOAD_RESEARCH_EVENT="upload_research"
const val DETAIL_SPECIES_EVENT="detail_species"
const val OPEN_RESEARCH_EVENT="open_research"

// Common params
const val PARAM_NAME="name"
const val PARAM_PLANT_NAME="plantName"
const val PARAM_TYPE="type"
const val PARAM_START_TIME="startTime"
const val PARAM_END_TIME="endTime"
const val PARAM_START_DATE="startDate"
const val PARAM_END_DATE="endDate"
const val PARAM_METERS="meters"
const val PARAM_SPEED="speed"
const val PARAM_INTERVAL="intervalChange"
const val PARAM_TIME="time"
const val PARAM_LAT="latitude"
const val PARAM_LON="longitude"
const val PARAM_TARGET_LAT="targetLatitude"
const val PARAM_TARGET_LON="targetLongitude"
const val PARAM_INTO_GARDEN="intoGarden"
const val PARAM_IS_CONNECTED="isConnected"
const val PARAM_BOOL_ANSWER="answer"
const val PARAM_IS_INSIDE="is_inside"
const val PARAM_ID="id"
const val PARAM_LINK="link"
const val PARAM_GENDER="gender"
const val PARAM_COUNTRY="country"
const val PARAM_RATING="rating"





// Functions
fun FirebaseAnalytics.reportEvent(title:String, content: Bundle){
    Log.d(FIREBASE_ANALYTICS_TAG,"$title:$content")
    this.logEvent(title, content)
}

