package com.botanical.garden.tablet.ui.forms


import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.ui.main.MainActivity

import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_suggestions.view.*

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.HashMap
import java.util.Locale

/**
 * A fragment with a Google +1 button.
 * Use the [SuggestionsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SuggestionsFragment : Fragment() {


    private var visitDateEditText: EditText?=null
    private var ratingBar: RatingBar?=null
    private var sendBtn: Button?=null
    private var suggestionsEditText: EditText?=null


    private var mDatabase: DatabaseReference? = null


    private val myCalendar = Calendar.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_suggestions, container, false)

        visitDateEditText = view.visit_date_edit_text
        ratingBar = view.rating_bar
        suggestionsEditText = view.suggestions_edit_text
        sendBtn = view.send_btn


        FirebaseApp.initializeApp(context!!)



        mDatabase = FirebaseDatabase.getInstance().reference


        sendBtn?.setOnClickListener {
            // Access a Cloud Firestore instance from your Activity
            val db = FirebaseFirestore.getInstance()

            val suggestion = HashMap<String, Any>()
            suggestion["experienceLevel"] = ratingBar!!.numStars
            suggestion["suggestion"] = suggestionsEditText?.text.toString()
            suggestion["visitDate"] = myCalendar.time.toString()

            db.collection("suggestions").add(suggestion).addOnSuccessListener {
                context?.createDialogMessage("Sugerencia enviada con éxito!","Gracias por contactarnos y ayudar a mejorar este jardín",null)
                    if(activity!=null)
                        (activity as MainActivity).backstackFragment(null)
                }
                .addOnFailureListener {
                    context?.createDialogMessage("Ha ocurrido un error","Disculpe la molestia, por favor inténtelo de nuevo más tarde") {
                        if(activity!=null)
                            (activity as MainActivity).backstackFragment(null)
                    }
                }
        }


        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        visitDateEditText?.setOnClickListener {
            // TODO Auto-generated method stub
            DatePickerDialog(context!!, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }



        return view
    }

    private fun updateLabel() {
        val myFormat = "MM/dd/yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        visitDateEditText?.setText(sdf.format(myCalendar.time))
    }

    override fun onDestroyView() {
        visitDateEditText=null
        ratingBar=null
        sendBtn=null
        suggestionsEditText=null


        mDatabase = null
        super.onDestroyView()
    }

    override fun onDestroy(){
        visitDateEditText=null
        ratingBar=null
        sendBtn=null
        suggestionsEditText=null


        mDatabase = null
        super.onDestroy()
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        fun newInstance(): SuggestionsFragment {
            return SuggestionsFragment()
        }
    }


}// Required empty public constructor
