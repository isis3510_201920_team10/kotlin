package com.botanical.garden.tablet.ui.search


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.EventModelNew
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.event_item.view.*

class EventAdapter(private val c: Context, data: Array<EventModelNew>, private val fragment: SearchFragmentJ) : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

    var data: Array<EventModelNew>?

    private val mInflater: LayoutInflater

    internal var eventClickListener: EventClickListener

    init {

        this.mInflater = LayoutInflater.from(c)
        this.data = data
        this.eventClickListener = fragment


    }

    fun filter(newEvents: Array<EventModelNew>?) {
        data = newEvents
        notifyDataSetChanged()
    }


    // inflates the cell layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventAdapter.EventViewHolder {
        val view = LayoutInflater.from(this.c).inflate(R.layout.event_item, parent, false)

        return EventViewHolder(view)

    }

    override fun onBindViewHolder(holder: EventAdapter.EventViewHolder, position: Int) {

        holder.eventName.text = data!![position].name
        Glide.with(fragment)
            .load(data!![position].imageUrl)
            .error(R.drawable.fallback)
            .fallback(R.drawable.fallback)
            .placeholder(R.drawable.fallback)
            .centerCrop().into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return data!!.size
    }


    inner class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var imageView: ImageView
        internal var eventName: TextView


        init {
            imageView = view.event_image
            eventName = view.event_name


            view.setOnClickListener { view -> fragment.onItemClick(view, adapterPosition) }
        }

    }

    interface EventClickListener {
        fun onItemClick(view: View, position: Int)
    }
}

