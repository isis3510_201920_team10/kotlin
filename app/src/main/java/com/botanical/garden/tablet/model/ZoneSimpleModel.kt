package com.botanical.garden.tablet.model

data class ZoneSimpleModel(
    val name:String,
    val imageUrl:String
)

