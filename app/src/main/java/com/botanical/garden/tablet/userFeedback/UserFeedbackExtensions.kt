package com.botanical.garden.tablet.userFeedback

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar


fun View.createSnackbarMessage(msg:String){
    val snackbar = Snackbar
        .make(this, msg, Snackbar.LENGTH_INDEFINITE)
        .setAction("OK") {}
    snackbar.show()
}


fun Context.createToastMessage(msg:String){
    val toast= Toast.makeText(this,
        msg, Toast.LENGTH_SHORT)
    toast.show()
}

fun Context.createDialogMessage(title:String, msg:String, okCallback:(()->Unit)?){
    val builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    builder.setMessage(msg)

    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
        okCallback?.invoke()
    }
    builder.setCancelable(false)

    builder.show()
}