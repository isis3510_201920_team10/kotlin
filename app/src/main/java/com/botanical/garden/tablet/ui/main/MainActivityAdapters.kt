package com.botanical.garden.tablet.ui.main

import android.view.View
import androidx.databinding.BindingAdapter
import com.botanical.garden.tablet.R
import com.google.android.material.snackbar.Snackbar


var snackbar:Snackbar?=null

@BindingAdapter("connectivityVisibility")
fun setConnectionSnackbar(view: View, isConnected:Boolean)
{
    if(!isConnected)
    {
        snackbar = Snackbar
            .make(view, view.context.getString(R.string.offline_msg), Snackbar.LENGTH_INDEFINITE)
            .setAction("OK") {}
        snackbar?.show()
    }
    else
    {
        if(snackbar!=null)
        {
            snackbar?.dismiss()
            snackbar=null
        }
    }
}