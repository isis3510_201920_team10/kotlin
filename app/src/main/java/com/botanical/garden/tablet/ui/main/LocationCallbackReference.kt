package com.botanical.garden.tablet.ui.main

import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationCallback
import java.lang.ref.WeakReference


class LocationCallbackReference internal constructor(locationCallback: LocationCallback) :
    LocationCallback() {

    private val locationCallbackRef: WeakReference<LocationCallback>?

    init {
        locationCallbackRef = WeakReference<LocationCallback>(locationCallback)
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        super.onLocationResult(locationResult)
        if (locationCallbackRef != null && locationCallbackRef!!.get() != null) {
            locationCallbackRef.get()?.onLocationResult(locationResult)
        }
    }
}