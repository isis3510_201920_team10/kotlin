package com.botanical.garden.tablet.repository

import android.content.res.AssetManager
import android.util.Log
import com.botanical.garden.tablet.model.*
import com.botanical.garden.tablet.utils.getDistance
import com.botanical.garden.tablet.utils.isInsideArea
import com.botanical.garden.tablet.utils.test
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.*

private const val CLOSE_ITEMS_TAG="CloseItems"
private const val ZONE_REPOSITORY="ZoneRepository"
private const val ZONE_TAG="CloseZone"

class ZoneRepository (val assetManager: AssetManager){

    init{
        startRepo()
    }

    var zoneData:Array<ZoneModel>?=null
    var eventData:Array<EventModelNew>?=null

    private fun readFile(path:String):String{
        val inputStream = assetManager.open(path)
        return inputStream.bufferedReader().use(BufferedReader::readText)
    }

    private fun startRepo(){
        val gson= Gson()

        var inputString=readFile("data/zones.json")
        if(inputString=="")
        {
            Log.d(ZONE_REPOSITORY,"Sorry, no zones file\n") // TODO Add error msg
            return
        }

        print("$inputString \n")
        zoneData = gson.fromJson(inputString, Array<ZoneModel>::class.java)

        inputString=readFile("data/events.json")
        if(inputString=="")
        {
            Log.d(ZONE_REPOSITORY,"Sorry, no events file\n") // TODO Add error msg
            return
        }

        print("$inputString \n")
        eventData = gson.fromJson(inputString, Array<EventModelNew>::class.java)
    }

    suspend fun getItemsCloseToPosition(position: LatLng,radius:Float ): List<PlantModelNew>{
        if(zoneData==null)
            return listOf()

        return withContext(Dispatchers.IO){
            val ans= mutableListOf<PlantModelNew>()
            zoneData?.forEach { zone->
                zone.plants.forEach {
                    if(getDistance(position,LatLng(it.latitude,it.longitude))<radius)
                    {
                        ans.add(it)
                    }
                }
            }
            Log.d(CLOSE_ITEMS_TAG,"Success")
            ans
        }
    }

    suspend fun getZoneCloseToPosition(position: LatLng):ZoneSimpleModel {
        if (zoneData == null)
            return ZoneSimpleModel("Zona no reconocida","")

        var area: LatLngBounds?
        var northeast: LatLng
        var southwest: LatLng

        Log.d(ZONE_TAG, "Zone dispatch start")

        return withContext(Dispatchers.IO) {
            zoneData?.forEach { zone ->
                Log.d(ZONE_TAG, "Inside the zone")

                if (test(position.latitude, position.longitude, zone.limits)) {
                    Log.d(ZONE_TAG, "Zone test ${zone.name}")
                    return@withContext zone.getSimpleModel()
                }

                northeast = zone.limits[1] //NORTHEAST DEFINED IN JSON STRUCT
                southwest = zone.limits[3] // SOUTHWEST DEFINED IN JSON STRUCT
                area = LatLngBounds(southwest, northeast)
                area?.let {
                    if (isInsideArea(position.latitude, position.longitude, it)) {
                        Log.d(ZONE_TAG, "Zone ${zone.name}")
                        return@withContext zone.getSimpleModel()
                    }
                }
            }

            Log.d(ZONE_TAG, "No zone")
            return@withContext ZoneSimpleModel("Zona no reconocida","")
        }
    }
}