package com.botanical.garden.tablet.ui.forms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.botanical.garden.tablet.R
import kotlinx.android.synthetic.main.forms.view.*

class FormsFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.forms, container, false)

        val suggestionsFragment = view.suggestionsCardView as CardView

        suggestionsFragment.setOnClickListener {
            findNavController().navigate(R.id.suggestionsCardView)
        }

        val guideOpinionFragment = view.guideOpinionCardView as CardView

        guideOpinionFragment.setOnClickListener {
            findNavController().navigate(R.id.guideOpinionCardView)
        }

        val eventOpinionFragment = view.eventOpinionCardView as CardView

        eventOpinionFragment.setOnClickListener {
            findNavController().navigate(R.id.eventOpinionCardView)
        }

        return view
    }
}
