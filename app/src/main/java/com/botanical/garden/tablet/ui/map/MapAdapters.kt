package com.botanical.garden.tablet.ui.map

import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import com.botanical.garden.tablet.model.Status
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*

private const val TAG="MapAdapters"

// PAttern config for painting
private const val PATTERN_GAP=10.0f

val patternDotted= listOf(Dot(), Gap(PATTERN_GAP))

// Used functions
fun List<com.google.maps.model.LatLng>.toLatLng():List<LatLng>
{
    return map { LatLng(it.lat,it.lng) }
}

private var oldRoute:Polyline?=null
private var oldMarker:Marker?=null

@BindingAdapter("endLatLng","route",requireAll = true)
fun setPath(mapView: MapView, endLatLng: LiveData<Pair<LatLng?,LatLng?>>?, route:LiveData<List<LatLng>?>?) {

    Log.d(TAG,"End $endLatLng, ${endLatLng?.value} and route $route")

    if(endLatLng==null || route==null)
    {
        oldMarker=null
        return
    }

    val pair= endLatLng.value ?: return
    if(pair.first==null)
        return
    Log.d(TAG,"Pair ${pair.first} ${pair.second}")


    val end=pair.second?:return

    var removeMarker=false
    var newMarker=true

    val points= route.value ?: listOf()
    if(points.isEmpty())
    {
        Log.d(TAG,"Empty route")
        removeMarker=true
        newMarker=false
    }

    val oldPosition= oldMarker?.position
    oldPosition?.let{
        if(it.latitude!=end.latitude && it.longitude!=end.longitude)
        {
            removeMarker=true
            Log.d(TAG,"Different lat long")
        }
        if(it.latitude==end.latitude && it.longitude==end.longitude)
        {
            newMarker=false
            Log.d(TAG,"Same lat long")
        }
    }

    val options=PolylineOptions().color(Color.BLUE).pattern(patternDotted)
        .addAll(points)

    mapView.getMapAsync{ map->
        Log.d(TAG,"REmoving route ${oldRoute?.points}")
        oldRoute?.remove()
        Log.d(TAG,"REmoving pos ${oldMarker?.position} VS $end. Remove marker? $removeMarker. New marker? $newMarker")
        if(removeMarker)
        {
            oldMarker?.remove()
            oldMarker=null
        }

        if(newMarker)
        {
            oldMarker=map.addMarker(MarkerOptions().position(end))
        }
        oldRoute=map.addPolyline(options)
        oldRoute?.zIndex=1000.0f
        Log.d(TAG,"Reposition ${oldRoute?.points}")
    }
}

@BindingAdapter("msgVisibility")
fun setMapMessage(view: TextView, statusData: LiveData<Status>?)
{
    if(statusData==null || statusData.value==null)
        return

    val status=statusData.value
    if(status==Status.OFFLINE || status==Status.LOADING || status==Status.ARRIVED)
        view.visibility= View.VISIBLE
    else
        view.visibility=View.GONE
}