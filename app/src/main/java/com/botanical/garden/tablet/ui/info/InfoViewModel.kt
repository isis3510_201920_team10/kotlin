package com.botanical.garden.tablet.ui.info

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModel
import com.botanical.garden.tablet.ui.main.MainActivity


class InfoViewModel : ViewModel() {
    private val _title = "Bienvenido a una nueva manera de vivir el Jardín"
    private val _info1 = arrayOf("Interactivo","Consulta información sobre las especies cercanas y ubica aquellas que quieres detallar. Tienes a tu disposición toda la información del jardín, en la palma de tu mano")
    private val _info2 =  arrayOf("A tu ritmo","Planea tu recorrido y tómate tu tiempo para descansar y aprender más sobre las especies del jardín: sin prisa y sin apuros. Disfruta del jardín a tu manera")

    val title=_title
    val info1=_info1
    val info2=_info2

    fun start(view: View) {
        val context = view.context
        val intent = Intent(context, MainActivity::class.java)
        context.startActivity(intent)
    }
}