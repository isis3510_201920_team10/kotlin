package com.botanical.garden.tablet.model

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlantModelNew(
    override var id: String="",

    override val name:String,

    override val imageUrl: String,

    val scientificName:String,

    override val description: String,

    override val type:DetailType=DetailType.PLANT,

    override val features: Map<String,String>,

    override val latitude: Double,

    override val longitude: Double,

    val links:Array<String?>


): Parcelable,ItemModel() {
    override fun toDetailModel(): LiveData<DetailModelNew> {
        val ans= MutableLiveData<DetailModelNew>()

        val detail=DetailModelNew(
            id,
            imageUrl,
            name,
            type,
            description,
            features,
            latitude,
            longitude,
            links,
            null,
            null
        )

        ans.postValue(detail)
        return ans
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PlantModelNew

        if (id != other.id) return false
        if (name != other.name) return false
        if (imageUrl != other.imageUrl) return false
        if (scientificName != other.scientificName) return false
        if (description != other.description) return false
        if (type != other.type) return false
        if (features != other.features) return false
        if (latitude != other.latitude) return false
        if (longitude != other.longitude) return false
        if (!links.contentEquals(other.links)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + imageUrl.hashCode()
        result = 31 * result + scientificName.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + features.hashCode()
        result = 31 * result + latitude.hashCode()
        result = 31 * result + longitude.hashCode()
        result = 31 * result + links.contentHashCode()
        return result
    }
}

