package com.botanical.garden.tablet.ui.detail

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Button
import android.widget.EditText
import androidx.core.os.bundleOf
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.DetailModelNew
import com.botanical.garden.tablet.ui.main.MainActivity

import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.botanical.garden.tablet.utils.*
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_scientific_article.view.*

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.HashMap
import java.util.Locale

/**
 * A fragment with a Google +1 button.
 * Use the [PostArticleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PostArticleFragment : Fragment() {
    //Analytics
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private var postDateEditText: EditText?=null
    private var documentEditText: EditText?=null
    private var sendBtn: Button?=null
    private var articleTitleEditText: EditText?=null
    private var articleLinkEditText: EditText?=null


    private var mDatabase: DatabaseReference? = null


    private val myCalendar = Calendar.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        val plantName=arguments?.getString("plantName")

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_scientific_article, container, false)

        postDateEditText = view.post_date_edit_text
        documentEditText = view.document_edit_text
        articleTitleEditText = view.article_title_edit_text
        articleLinkEditText = view.article_link_edit_text
        sendBtn = view.send_btn

        if(context==null)
            return view

        // Firebase initialization
        context?.let{
            firebaseAnalytics = FirebaseAnalytics.getInstance(it)
        }

        FirebaseApp.initializeApp(context!!)
        mDatabase = FirebaseDatabase.getInstance().reference


        sendBtn?.setOnClickListener {
            if (!URLUtil.isValidUrl(articleLinkEditText?.text.toString())){
                context?.createDialogMessage("Link inválido!","Por favor ingresar un link válido") {
                    if(activity!=null)
                        (activity as MainActivity).backstackFragment(null)
                }
            } else if (!checkIfDocIsValid(documentEditText?.text.toString())) {
                context?.createDialogMessage("Documento no registrado!","Lo sentimos pero su documento no está registrado, por favor contáctenos para registrarlo") {
                    if(activity!=null)
                        (activity as MainActivity).backstackFragment(null)
                }
            } else {
                // Access a Cloud Firestore instance from your Activity
                val db = FirebaseFirestore.getInstance()

                val article = HashMap<String, Any>()
                article["document"] = documentEditText?.text.toString()
                article["articleTitle"] = articleTitleEditText?.text.toString()
                article["articleLink"] = articleLinkEditText?.text.toString()
                article["postDate"] = myCalendar.time.toString()

                db.collection("articles").add(article).addOnSuccessListener {
                    firebaseAnalytics.reportEvent(UPLOAD_RESEARCH_EVENT,bundleOf(PARAM_ID to article["document"],
                        PARAM_NAME to article["articleTitle"], PARAM_LINK to article["articleLink"], PARAM_PLANT_NAME to plantName))

                    context?.createDialogMessage("Artículo enviado con éxito!","Gracias por contactarnos y ayudar a mejorar este jardín") {
                        if(activity!=null)
                            (activity as MainActivity).backstackFragment(null)
                    }

                }
                    .addOnFailureListener {
                        context?.createDialogMessage("Ha ocurrido un error","Disculpe la molestia, por favor inténtelo de nuevo más tarde") {
                            if(activity!=null)
                                (activity as MainActivity).backstackFragment(null)
                        }
                    }
            }
        }


        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        postDateEditText?.setOnClickListener {
            // TODO Auto-generated method stub
            DatePickerDialog(context!!, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }



        return view
    }

    private fun checkIfDocIsValid(documentEditText: String): Boolean {
        val validDocumentEditTexts: Array<String> = arrayOf("1127025393", "1016101372")

        for (validDocumentEditText in validDocumentEditTexts) {
            if (documentEditText == validDocumentEditText){
                return true
            }
        }

        return  false
    }

    private fun updateLabel() {
        val myFormat = "MM/dd/yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        postDateEditText?.setText(sdf.format(myCalendar.time))
    }

    override fun onDestroyView() {
        postDateEditText=null
        documentEditText=null
        sendBtn=null
        articleTitleEditText=null
        articleLinkEditText=null


        mDatabase = null
        super.onDestroyView()
    }

    override fun onDestroy(){
        postDateEditText=null
        documentEditText=null
        sendBtn=null
        articleTitleEditText=null
        articleLinkEditText=null


        mDatabase = null
        super.onDestroy()
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        fun newInstance(): PostArticleFragment {
            return PostArticleFragment()
        }
    }


}// Required empty public constructor

