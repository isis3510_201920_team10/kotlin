package com.botanical.garden.tablet.model

import com.google.android.gms.maps.model.LatLng

data class ZoneModel(
    val name:String,

    val imageUrl: String,

    val limits:Array<LatLng>,

    var plants: Array<PlantModelNew>,

    val places: Array<PlaceModelNew>,

    val features: Map<String,ArrayList<String>>
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ZoneModel

        if (name != other.name) return false
        if (imageUrl != other.imageUrl) return false
        if (!limits.contentEquals(other.limits)) return false
        if (!plants.contentEquals(other.plants)) return false
        if (!places.contentEquals(other.places)) return false
        if (features != other.features) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + imageUrl.hashCode()
        result = 31 * result + limits.contentHashCode()
        result = 31 * result + plants.contentHashCode()
        result = 31 * result + places.contentHashCode()
        result = 31 * result + features.hashCode()
        return result
    }

    fun getSimpleModel():ZoneSimpleModel{
        return ZoneSimpleModel(this.name,this.imageUrl)
    }
}

