package com.botanical.garden.tablet.model

enum class DetailType {
    PLANT,
    EVENT,
    PLACE
}