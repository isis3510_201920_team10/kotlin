package com.botanical.garden.tablet.ui.forms


import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.ui.main.MainActivity

import com.botanical.garden.tablet.userFeedback.createDialogMessage
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_exit_garden.view.*
import kotlinx.android.synthetic.main.fragment_suggestions.view.*
import kotlinx.android.synthetic.main.fragment_suggestions.view.rating_bar
import kotlinx.android.synthetic.main.fragment_suggestions.view.send_btn

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.botanical.garden.tablet.utils.*


/**
 * A fragment with a Google +1 button.
 * Use the [ExitGardenFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ExitGardenFragment : Fragment() {


    private var ratingBar: RatingBar?=null
    private var genderSpinner: Spinner?=null
    private var countrySpinner: Spinner?=null

    private var sendBtn: Button?=null


    private var firebaseAnalytics: FirebaseAnalytics? = null


    private val myCalendar = Calendar.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_exit_garden, container, false)

        genderSpinner = view.gender_spinner
        ratingBar = view.rating_bar
        countrySpinner = view.country_spinner
        sendBtn = view.send_btn

        setSpinners()

        firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)


        sendBtn?.setOnClickListener {
            val bundle = bundleOf(PARAM_RATING to ratingBar?.numStars,
                PARAM_GENDER to genderSpinner?.selectedItem,
                PARAM_COUNTRY to countrySpinner?.selectedItem)

            firebaseAnalytics?.reportEvent(EXIT_GARDEN_EVENT,bundle)

            context?.createDialogMessage(getString(R.string.form_opinion_success_title),
                getString(R.string.form_opinion_success_msg)){
                    findNavController().navigate(R.id.bottom_navigation_item_home)
                }
        }

        return view
    }

    private fun setSpinners(){
        // Genders

        val genderAdapter = ArrayAdapter(
            context!!,
            R.layout.simple_spinner_item, listOf(getString(R.string.gender_male),
                getString(R.string.gender_female),
                getString(R.string.gender_indefinite))
        )

        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the your spinner
        genderSpinner?.adapter = genderAdapter

        // Countries
        val locales = Locale.getAvailableLocales()
        val countries = ArrayList<String>()
        for (locale in locales) {
            val country = locale.displayCountry
            if (country.trim { it <= ' ' }.isNotEmpty() && !countries.contains(country)) {
                countries.add(country)
            }
        }

        countries.sort()

        val countryAdapter = ArrayAdapter(
            context!!,
            R.layout.simple_spinner_item, countries
        )

        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the your spinner
        countrySpinner?.adapter = countryAdapter
    }


    override fun onDestroyView() {
        genderSpinner=null
        countrySpinner=null
        ratingBar=null
        sendBtn=null
        firebaseAnalytics=null

        super.onDestroyView()
    }

    override fun onDestroy(){
        genderSpinner=null
        countrySpinner=null
        ratingBar=null
        sendBtn=null
        firebaseAnalytics=null

        super.onDestroy()
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        fun newInstance(): ExitGardenFragment {
            return ExitGardenFragment()
        }
    }


}// Required empty public constructor
