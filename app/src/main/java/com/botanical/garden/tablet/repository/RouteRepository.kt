package com.botanical.garden.tablet.repository

import android.util.Log
import com.botanical.garden.tablet.BuildConfig
import com.botanical.garden.tablet.model.Resource
import com.botanical.garden.tablet.ui.map.toLatLng
import com.google.android.gms.maps.model.LatLng
import com.google.maps.GeoApiContext
import com.google.maps.model.TravelMode
import com.google.maps.DirectionsApi
import com.google.maps.internal.PolylineEncoding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

private const val TAG="RouteRepository"


class RouteRepository {

    private val geoApiContext = GeoApiContext.Builder().apiKey(BuildConfig.ApiKey).queryRateLimit(1).build()

    private fun getGoogleRoute(origin: LatLng, destination:LatLng): List<LatLng>?{
        val newOrigin=com.google.maps.model.LatLng(origin.latitude,origin.longitude)
        val newDestination=com.google.maps.model.LatLng(destination.latitude,destination.longitude)
        val route = try {
            val response=DirectionsApi.newRequest(geoApiContext).mode(TravelMode.WALKING).origin(newOrigin)
                .destination(newDestination).await()
            Log.d(TAG,"API Response successful")
            response.routes[0]
        } catch(e:Exception) {
            Log.e(TAG, "I got an error", e)
            null
        } ?: return null

        val list=PolylineEncoding.decode(route.overviewPolyline.encodedPath)
        list.add(0,com.google.maps.model.LatLng(origin.latitude,origin.longitude))
        list.add(com.google.maps.model.LatLng(destination.latitude,destination.longitude))

        return list.toLatLng()
    }

    private fun getOfflineRoute(origin:LatLng,destination:LatLng) : List<LatLng>?{
        //FIXME: Add logic through an alg like Dijkstra or so
        val newOrigin=com.google.maps.model.LatLng(origin.latitude,origin.longitude)
        val newDestination=com.google.maps.model.LatLng(destination.latitude,destination.longitude)

        val path=listOf(newOrigin,newDestination)
        return path.toLatLng()
    }

    suspend fun getRoute(origin:LatLng?,destination: LatLng?) : Resource<List<LatLng>?> {
        if(origin==null || destination==null)
        {
            return Resource.error("La ruta no se ha definido aún",null)
        }
        val ans=withContext(Dispatchers.IO){
            // Try google, then offline path
            var path=getGoogleRoute(origin,destination)
            Log.d(TAG,"Returned online path $path")

            val isOffline=(path==null)
            if(isOffline)
            {
                path=getOfflineRoute(origin,destination)
            }

            Log.d(TAG,"Path $origin VS $path")
            // Return zoneData depending on the status
            val data= path
            if(isOffline){
                Resource.offline("No podemos obtener la ruta dada :O, mientras tanto mostraremos rutas muy imprecisas (una línea recta) para guiarte.",data)
            } else {
                Resource.success(data)
            }
        }
        return ans

    }
}