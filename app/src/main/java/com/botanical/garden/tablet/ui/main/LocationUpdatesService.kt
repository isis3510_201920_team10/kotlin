package com.botanical.garden.tablet.ui.main

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.Binder
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.os.Looper
import android.util.Log

import androidx.localbroadcastmanager.content.LocalBroadcastManager

import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

import com.botanical.garden.tablet.utils.similarDistance
import com.botanical.garden.tablet.utils.requestingLocationUpdates
import com.botanical.garden.tablet.utils.setRequestingLocationUpdates


private const val INITIAL_INTERVAL = 2L
private const val MAX_INTERVAL = 5 * 60L
private const val MAX_RETRIES: Long = 3
private const val MEASURE_UNIT = 1000L

private var currentInterval = INITIAL_INTERVAL
private var counter = MAX_RETRIES


//
// BAsed on the implementation from shantanu GitHub user at https://github.com/BlackBlind567/Location_Updates-Background_Foreground/tree/master/app/src/main
// The idea is to make a service that runs on the MainActivity and is always listening to location events, with a dynamic approach
//
class LocationUpdatesService : Service() {

    private val mBinder = LocalBinder()


    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private var mChangingConfiguration = false
    /**
     * Contains parameters used by [com.google.android.gms.location.FusedLocationProviderApi].
     */
    private var mLocationRequest: LocationRequest? = null

    /**
     * Provides access to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Callback for changes in location.
     */
    private var mLocationCallback: LocationCallback? = null

    private var mServiceHandler: Handler? = null

    internal var latitude: Double? = null
    internal var longitude: Double? = null

    /**
     * The current location.
     */
    private var mLocation: Location? = null

    override fun onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult.lastLocation)
            }
        }

        createLocationRequest()
        getLastLocation()

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
    }


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(TAG, "Service started")
        val startedFromNotification = intent.getBooleanExtra(
            EXTRA_STARTED_FROM_NOTIFICATION,
            false
        )

        // We got here because the user decided to remove location updates from the notification.
        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }
        // Tells the system to not try to recreate the service after it has been killed.
        return Service.START_NOT_STICKY
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mChangingConfiguration = true
    }


    override fun onBind(intent: Intent): IBinder? {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
        Log.i(TAG, "in onBind()")
        stopForeground(true)
        mChangingConfiguration = false

        return mBinder
    }


    override fun onRebind(intent: Intent) {
        // Called when a client (MainActivity in case of this sample) returns to the foreground
        // and binds once again with this service. The service should cease to be a foreground
        // service when that happens.
        Log.i(TAG, "in onRebind()")
        stopForeground(true)
        mChangingConfiguration = false


        super.onRebind(intent)
    }


    override fun onUnbind(intent: Intent): Boolean {
        Log.i(TAG, "Last client unbound from service")

        // Called when the last client (MainActivity in case of this sample) unbinds from this
        // service. If this method is called due to a configuration change in MainActivity, we
        // do nothing. Otherwise, we make this service a foreground service.
        if (!mChangingConfiguration && requestingLocationUpdates(this)) {
            Log.d(TAG, "Starting foreground service")
            /*
            // TODO(developer). If targeting O, use the following code.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                mNotificationManager.startServiceInForeground(new Intent(this,
                        LocationUpdatesService.class), NOTIFICATION_ID, getNotification());
            } else {
                startForeground(NOTIFICATION_ID, getNotification());
            }
             */

        }
        return true // Ensures onRebind() is called when a client re-binds.
    }


    override fun onDestroy() {
        mServiceHandler!!.removeCallbacksAndMessages(null)
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun requestLocationUpdates(serviceStart: Boolean) {
        Log.i(TAG, "Requesting location updates $currentInterval")
        setRequestingLocationUpdates(this, true)
        if (serviceStart)
            startService(Intent(applicationContext, LocationUpdatesService::class.java))
        try {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback!!, Looper.myLooper()
            )
        } catch (unlikely: SecurityException) {
            setRequestingLocationUpdates(this, false)
            Log.d(TAG, "Lost location permission. Could not request updates. $unlikely")
        }

    }


    private fun getLastLocation() {
        try {
            mFusedLocationClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        Log.d(TAG, "Location retrieved")
                        mLocation = task.result
                    } else {
                        Log.w(TAG, "Failed to get location.")
                    }
                }
        } catch (unlikely: SecurityException) {
            Log.d(TAG, "Lost location permission.$unlikely")
        }

    }

    private fun onNewLocation(location: Location) {
        if(mLocation==null)
        {
            mLocation=location
            return
        }

        Log.d(TAG, "Prev ${mLocation!!.latitude} ${mLocation!!.longitude}  New location: ${location.latitude} ${location.longitude} counter $counter")
        var remove = false

        if (similarDistance(
                location.latitude,
                mLocation!!.latitude
            ) && similarDistance(location.longitude, mLocation!!.longitude)
        ) {
            Log.d(TAG, "Still")
            counter--
        } else {
            Log.d(TAG, "Retry")
            counter = MAX_RETRIES
            currentInterval = INITIAL_INTERVAL
            remove = true
        }

        if (counter <= 0) {
            Log.d(TAG, "Max retry\n")
            counter = MAX_RETRIES

            if (currentInterval < MAX_INTERVAL)
                currentInterval *= 2
            else {
                currentInterval = INITIAL_INTERVAL
            }
            remove = true
        }

        mLocation = location

        // Notify anyone listening for broadcasts about the new location.
        val intent = Intent(ACTION_BROADCAST)
        intent.putExtra(EXTRA_LOCATION, location)
        intent.putExtra(EXTRA_INTERVAL, currentInterval)
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

        if (remove) {
            restartLocationUpdates()
        }
    }

    /**
     * Sets the location request parameters.
     */
    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = currentInterval * MEASURE_UNIT
        mLocationRequest!!.fastestInterval = currentInterval * MEASURE_UNIT
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun removeLocationUpdates() {
        Log.i(TAG, "Removing location updates")
        try {
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback!!)
            setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            setRequestingLocationUpdates(this, true)
            Log.d(TAG, "Lost location permission. Could not remove updates. $unlikely")
        }

    }

    fun restartLocationUpdates() {
        Log.i(TAG, "Removing location updates")
        try {
            val task = mFusedLocationClient!!.removeLocationUpdates(mLocationCallback!!)
            task.addOnSuccessListener {
                Log.d(TAG, "Success cancelling task")
                mLocationCallback = object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult) {
                        super.onLocationResult(locationResult)
                        onNewLocation(locationResult.lastLocation)
                    }
                }

                createLocationRequest()
                requestLocationUpdates(false)
            }
            task.addOnFailureListener {
                // Task failed with an exception
                // ...
                Log.d(TAG, "Error cancelling task")
            }
            setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            setRequestingLocationUpdates(this, true)
            Log.d(TAG, "Lost location permission. Could not remove updates. $unlikely")
        }

    }

    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        internal val service: LocationUpdatesService
            get() = this@LocationUpdatesService
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The [Context].
     */
    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service in manager.getRunningServices(
            Integer.MAX_VALUE
        )) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    companion object {

        private val PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationupdatesforegroundservice"

        private val TAG = "resPOINT"

        /**
         * The name of the channel for notifications.
         */
        private val CHANNEL_ID = "channel_01"

        internal val ACTION_BROADCAST = "$PACKAGE_NAME.broadcast"

        internal val EXTRA_LOCATION = "$PACKAGE_NAME.location"
        internal val EXTRA_INTERVAL = "$PACKAGE_NAME.interval"
        private val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"
    }
}