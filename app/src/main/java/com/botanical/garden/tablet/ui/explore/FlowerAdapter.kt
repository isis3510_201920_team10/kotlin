package com.botanical.garden.tablet.ui.explore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.PlantModelNew
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.flowers_item.view.*

class FlowerAdapter(private val context: Context, private val fragment:ExploreFragmentJ) : RecyclerView.Adapter<FlowerAdapter.FlowerViewHolder>() {

    private var data: Array<PlantModelNew>? = null

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    var allData: Array<PlantModelNew>?=null


    fun setData(data: Array<PlantModelNew>?){
        this.data=data
        if(allData==null) {
            allData=data
        }
        notifyDataSetChanged()
    }


    // inflates the cell layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlowerViewHolder {
        val view = mInflater.inflate(R.layout.flowers_item, parent, false)
        return FlowerViewHolder(view)
    }

    override fun onBindViewHolder(holder: FlowerViewHolder, position: Int) {
        holder.scientificName.text = data!![position].scientificName
        holder.flowerName.text = data!![position].name
        Glide.with(context)
            .load(data!![position].imageUrl)
            .error(R.drawable.fallback)
            .fallback(R.drawable.fallback)
            .placeholder(R.drawable.fallback)
            .into(holder.imageView)

    }

    override fun getItemCount(): Int {
        if(data==null || data?.size==null)
            return 0

        return data!!.size
    }


    inner class FlowerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var imageView: ImageView = view.flower_image
        internal var flowerName: TextView = view.flower_name
        internal var scientificName: TextView = view.scientific_name


        init {

         view.setOnClickListener { fragment.onItemClick(it, adapterPosition) }

        }

    }

    fun getItem(position: Int): PlantModelNew {
        return data!![position]
    }


    interface PlantClickListener {
        fun onItemClick(view: View, position: Int)
    }
}
