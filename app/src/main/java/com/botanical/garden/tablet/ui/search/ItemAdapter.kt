package com.botanical.garden.tablet.ui.search


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.model.DetailModelNew
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.event_item.view.*

class ItemAdapter(private val c: Context, private val fragment: SearchFragmentJ) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    var data: Array<LiveData<DetailModelNew>>?
    var allData: Array<LiveData<DetailModelNew>>?

    private val mInflater: LayoutInflater

    internal var eventClickListener: EventAdapter.EventClickListener

    init {

        this.mInflater = LayoutInflater.from(c)
        this.data = arrayOf()
        this.eventClickListener = fragment
        this.allData = data

    }

    fun filter(newEvents: Array<LiveData<DetailModelNew>>?) {
        data = newEvents
        notifyDataSetChanged()
    }

    fun setInfo(data: Array<LiveData<DetailModelNew>>?){
        this.data=data
        this.allData=data
    }


    // inflates the cell layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ItemViewHolder {
        val view = LayoutInflater.from(this.c).inflate(R.layout.event_item, parent, false)

        return ItemViewHolder(view)

    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        if(data==null)
            return
        val value= data!![position].value ?: return

        holder.eventName.text = value!!.title
        Glide.with(fragment)
            .load(data!![position].value!!.img)
            .error(R.drawable.fallback)
            .fallback(R.drawable.fallback)
            .placeholder(R.drawable.fallback)
            .centerCrop().into(holder.imageView)

    }

    override fun getItemCount(): Int {
        if (data==null)
            return 0
        return data!!.size
    }


    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        internal var imageView: ImageView
        internal var eventName: TextView


        init {
            imageView = view.event_image
            eventName = view.event_name


            view.setOnClickListener { view -> fragment.onItemClick(view, adapterPosition) }
        }

    }

    fun getItem(position: Int): LiveData<DetailModelNew> {
        return data!![position]
    }
}

