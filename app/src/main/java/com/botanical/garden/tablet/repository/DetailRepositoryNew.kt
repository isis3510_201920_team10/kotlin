package com.botanical.garden.tablet.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.botanical.garden.tablet.model.*

class DetailRepositoryNew {

    val plantRepository=PlantRepositoryNew()
    val eventRepository=EventRepositoryNew()
    val placeRepository=PlaceRepositoryNew()

    suspend fun getAllDetails(context: Context): Array<LiveData<DetailModelNew>>
    {
        val plants: Array<PlantModelNew> = plantRepository.getAllPlants(context)

        val items: MutableList<LiveData<DetailModelNew>> = mutableListOf()

        for(i in 0 until plants.size){
            items.add(plants[i].toDetailModel())
        }

        val events: Array<EventModelNew> = eventRepository.getAllEvents(context)

        for(i in 0 until events.size){
            items.add(events[i].toDetailModel())
        }

        val places: Array<PlaceModelNew> = placeRepository.getAllPlaces(context)

        for(i in 0 until events.size){
            items.add(places[i].toDetailModel())
        }

        return items.toTypedArray()

    }

}