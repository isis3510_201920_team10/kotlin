package com.botanical.garden.tablet.ui.main

import android.Manifest
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat

import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.botanical.garden.tablet.BuildConfig
import com.botanical.garden.tablet.ui.explore.ExploreFragmentJ

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.ActivityMainBinding
import com.botanical.garden.tablet.model.PlantModelNew
import com.botanical.garden.tablet.repository.PlantRepositoryNew
import com.botanical.garden.tablet.ui.detail.PostArticleFragment
import com.botanical.garden.tablet.userFeedback.createDialogMessage

import com.botanical.garden.tablet.userFeedback.createSnackbarMessage
import com.botanical.garden.tablet.utils.*

import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.material.bottomnavigation.BottomNavigationView

import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

private const val TAG="MainActivity"
private const val LOCATION_TAG="MainActivityLocation"
private const val LOCATION_PERMISSION_REQUEST_CODE=30

private val BOTANICAL_GARDEN: LatLngBounds = LatLngBounds(
    LatLng(4.66593, -74.10414),LatLng(4.67035, -74.09656))



class MainActivity : AppCompatActivity(), ExploreFragmentJ.OnFragmentInteractionListener {

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    // View models
    private val viewModel by lazy { ViewModelProviders.of(this).get(MainActivityViewModel::class.java) }
    private val mapViewModel by lazy { ViewModelProviders.of(this).get(MapViewModel::class.java) }
    private val cardViewModel by lazy { ViewModelProviders.of(this).get(CardItemsViewModel::class.java) }

    // Offline connection and analytics
    private var isConnected=false
    private var enterTime:Long?=null

    // NAvigation
    private var navController: NavController? =null

    // Connectivity listener
    private var broadcastReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(ConnectivityManager
                .EXTRA_NO_CONNECTIVITY, false)
            if (notConnected) {
                viewModel.setIsConnected(false)
            } else {
                viewModel.setIsConnected(true)
            }
        }
    }

    private var myReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getParcelableExtra<Location>(LocationUpdatesService.EXTRA_LOCATION)
            val interval = intent.getLongExtra(LocationUpdatesService.EXTRA_INTERVAL,0)
            if (location != null) {
                Log.d(LOCATION_TAG,"Llegó ${location.latitude} ${location.longitude}")
                mapViewModel.setStart(LatLng(location.latitude, location.longitude))

                val isInsideGarden=isInsideArea(location.latitude,location.longitude, BOTANICAL_GARDEN)
                sendAnalyticsLocation(isInsideGarden,location.latitude,location.longitude,location.speed,interval)

                mapViewModel.setInsideGarden(isInsideGarden)
            }
        }
    }


    // MAp Listener

    // A reference to the service used to get location updates.
    private var mService:LocationUpdatesService? = null;

    // Tracks the bound state of the service.
    private var mBound = false;

    private var mServiceConnection : ServiceConnection? = object : ServiceConnection{

        override fun onServiceConnected(name:ComponentName, service:IBinder) {
            val binder = service as LocationUpdatesService.LocalBinder;
            mService = binder.service;
            mBound = true;
            mService?.requestLocationUpdates(true)
        }

        override fun onServiceDisconnected(name:ComponentName) {
            mService = null;
            mBound = false;
        }
    }



    override fun onFragmentInteraction(plant: PlantModelNew) {
        return
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setup main activity binding
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        // Setup location client
        requestPermission()

        isConnected=false
        this.let{
            val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            isConnected=if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                val network = connectivityManager.activeNetwork
                val capabilities = connectivityManager.getNetworkCapabilities(network)
                capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            } else {
                connectivityManager.activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
            }

            viewModel.setIsConnected(isConnected)
        }

        // Setup start fragment
        setupBottomTabNavigation()

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        // Answer questionnaire
        wifiQuestionnaire()

    }



    private fun wifiQuestionnaire(){
        Log.d(TAG,"Enter questionnaire")
        val prefs = getSharedPreferences(WIFI_QUESTIONNAIRE_STORE,Context.MODE_PRIVATE)
        val answered = prefs.getBoolean(WIFI_QUESTIONNAIRE_ANSWERED, false)
        if(answered){
            return
        }

        val editor = prefs.edit()
        val lastTime=prefs.getLong(WIFI_QUESTIONNAIRE_FIRST_DATE,-1)
        if (lastTime==-1L){
            editor.putLong(WIFI_QUESTIONNAIRE_FIRST_DATE,Date().time)
            editor.apply()
        }

        // Check delay time for comparison
        var delayTime=WIFI_QUESTIONNAIRE_TIME_RANGE
        if(BuildConfig.QuestionnaireTrigger){
            delayTime= WIFI_QUESTIONNAIRE_DEBUG_TIME_RANGE
        }

        if(lastTime==-1L || (Date().time-lastTime)< delayTime){
            if(BuildConfig.QuestionnaireTrigger){
                Handler().postDelayed({
                    wifiQuestionnaire()
                    //Do something after 100ms
                }, WIFI_QUESTIONNAIRE_DEBUG_DELAY)
            }
            return
        }



        val bundle=Bundle()

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.wifi_questionnaire))
        builder.setMessage(getString(R.string.wifi_questionnaire_content))


        builder.setNegativeButton("No") { dialog, which ->
            editor.putBoolean(WIFI_QUESTIONNAIRE_ANSWERED,false)
            bundle.putBoolean(PARAM_BOOL_ANSWER, false)
            firebaseAnalytics.reportEvent(WIFI_QUESTIONNAIRE_EVENT,bundle)
            editor.apply()
        }
        builder.setPositiveButton("Sí") { dialog, which ->
            editor.putBoolean(WIFI_QUESTIONNAIRE_ANSWERED,true)
            bundle.putBoolean(PARAM_BOOL_ANSWER, true)
            firebaseAnalytics.reportEvent(WIFI_QUESTIONNAIRE_EVENT,bundle)
            editor.apply()
        }
        builder.setCancelable(false)

        builder.show()
    }


    private fun sendAnalyticsLocation(isInsideGarden: Boolean, latitude: Double, longitude: Double, speed:Float, interval:Long) {
        var bundle=bundleOf(PARAM_LAT to latitude, PARAM_LON to longitude,
            PARAM_SPEED to speed, PARAM_INTERVAL to interval)

        firebaseAnalytics.reportEvent(USER_LOCATION_EVENT,bundle)

        if(enterTime==null && isInsideGarden){
            enterTime=Date().time
        }

        val intoGarden= (if(mapViewModel.insideGarden.value==false && isInsideGarden) {true }else if (mapViewModel.insideGarden.value==true && !isInsideGarden){ false }else null)
            ?: return
        bundle=bundleOf(PARAM_IS_CONNECTED to isConnected)
        bundle.putBoolean(PARAM_INTO_GARDEN,intoGarden)
        if(!intoGarden){
            val time=Date().time-enterTime!!
            bundle.putLong(PARAM_TIME,time)
            bundle.putFloat(PARAM_METERS,time/1000*speed)
            enterTime=null

            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.exit_garden_dialog_title))
            builder.setMessage(getString(R.string.exit_garden_dialog_msg))


            builder.setNegativeButton("No",null)
            builder.setPositiveButton("Sí") { dialog, which ->
                navController?.navigate(R.id.exitGardenFragment2)
            }
            builder.setCancelable(false)

            builder.show()
        }
        firebaseAnalytics.reportEvent(CHANGE_LOCATION_EVENT,bundle)


    }

    private fun requestPermission() {
        Log.d(TAG,"Permits ${ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)} && ${ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION)} VS ${PackageManager.PERMISSION_GRANTED}" )
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {
            // Permission has already been granted
            Log.d(LOCATION_TAG,"Service start $mService")
            mService?.requestLocationUpdates(true)
        }
    }


    private fun setupBottomTabNavigation() {
        val navView = findViewById<BottomNavigationView>(R.id.bottom_navigation_view)
        navController = Navigation.findNavController(this, R.id.main_container)
        NavigationUI.setupWithNavController(navView, navController!!)
        bottom_navigation_view.setOnNavigationItemReselectedListener {
            // Do nothing to ignore the reselection
        }
    }



    fun postArticle(v:View?, plantName:String) {
        PostArticleFragment.newInstance()
        val bundle = bundleOf("plantName" to plantName)

        v?.findNavController()?.navigate(R.id.post_article,bundle)
    }

    //------------
    // Life cycle methods
    //------------
    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))

        mServiceConnection?.let{
            bindService(Intent(this,LocationUpdatesService::class.java),it,
                Context.BIND_AUTO_CREATE);
        }

    }

    override fun onResume() {
        super.onResume()

        Log.d(LOCATION_TAG,"My Receiver starts $myReceiver")
        myReceiver?.let{
            LocalBroadcastManager.getInstance(this).registerReceiver(it,
                IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
        }
    }

    override fun onPause() {
        Log.d(LOCATION_TAG,"My Receiver $myReceiver")

        myReceiver?.let {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(it)
        }

        super.onPause()
        Log.d("XXXX","Pause")
    }

    override fun onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            mServiceConnection?.let{unbindService(it)}
            mBound = false;
        }

        super.onStop()
        if(broadcastReceiver!=null)
            unregisterReceiver(broadcastReceiver)
        broadcastReceiver=null
        navController=null

        Log.d("XXXX","Stop")

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG,"GOOD TO GO")
                mService?.requestLocationUpdates(true)
            } else {
                cl_main.createSnackbarMessage(getString(R.string.error_location_permission_denied))
            }
        }
    }

    //-----------------
    // UI Direct functions
    //-----------------
    fun goToMap(v: View?, lat:Double?, lng: Double?) {
        if(lat!=null && lng!=null)
            mapViewModel.setEnd(LatLng(lat, lng))
        else
            mapViewModel.setEnd(null)
        val bundle= bundleOf("isConnected" to isConnected)

        Log.d(LOCATION_TAG,"View for map? ${v!=null}")
        if(v!=null)
        {
            v.findNavController().navigate(R.id.navigation_map,bundle)
        }
        else
        {
            navController?.navigate(R.id.navigation_map,bundle)
        }

    }

    fun backstackFragment(v: View?) {
        if(v!=null)
            v.findNavController().popBackStack()
        else
            navController?.popBackStack()
    }

    fun setDetailId(id: Int?) {
        cardViewModel.setId(id)
    }

    fun goToQRDetail(){
        val plantRepositoryNew = PlantRepositoryNew()
        val plant=plantRepositoryNew.getPlantById(this,cardViewModel.id.value!!)!!

        val endLatLng=mapViewModel.endLatLngPoints.value
        var bundle = bundleOf(PARAM_NAME to plant.name, PARAM_LAT to endLatLng?.first?.latitude, PARAM_LON to endLatLng?.first?.longitude,
            PARAM_TARGET_LAT to plant.latitude, PARAM_TARGET_LON to plant.longitude)
        firebaseAnalytics.reportEvent(SCAN_QR_EVENT,bundle)

        bundle = bundleOf("plant" to plant)
        navController?.navigate(R.id.navigation_plant_detail,bundle)
    }

    fun goToExit(){
        mapViewModel.goToExit()
        val bundle= bundleOf("isConnected" to isConnected)
        val navController = Navigation.findNavController(this, R.id.main_container)
        navController?.navigate(R.id.navigation_map,bundle)
    }

    fun goToCloseItem(){
        val closeItems = mapViewModel.closeItems.value
        Log.d("XXXX","Get close items $closeItems")
        if (closeItems.isNullOrEmpty()) {
            goToMap(null, null, null)
            return
        }

        goToMap(null, closeItems[0].latitude, closeItems[0].longitude)
    }

    fun logLocationEvent(event:String,bundle:Bundle){
        val endLatLng=mapViewModel.endLatLngPoints.value
        val lon=endLatLng?.first?.longitude!!
        val lat=endLatLng.first?.latitude!!
        bundle.putDouble(PARAM_LAT,lat)
        bundle.putDouble(PARAM_LON, lon)
        bundle.putBoolean(PARAM_IS_INSIDE, isInsideArea(lat,lon, BOTANICAL_GARDEN))

        firebaseAnalytics.reportEvent(event,bundle)
    }
}
