package com.botanical.garden.tablet.ui.info

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.botanical.garden.tablet.BuildConfig
import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.ActivityInfoBinding
import com.botanical.garden.tablet.utils.WIFI_QUESTIONNAIRE_ANSWERED
import com.botanical.garden.tablet.utils.WIFI_QUESTIONNAIRE_FIRST_DATE
import com.botanical.garden.tablet.utils.WIFI_QUESTIONNAIRE_STORE
import java.util.*

class InfoActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProviders.of(this).get(InfoViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Change from splash
        setTheme(R.style.AppTheme_Regular)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val binding: ActivityInfoBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_info)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        if(BuildConfig.QuestionnaireErase){
            val prefs = getSharedPreferences(WIFI_QUESTIONNAIRE_STORE, Context.MODE_PRIVATE)
            val editor=prefs.edit()
            editor.remove(WIFI_QUESTIONNAIRE_ANSWERED)
            editor.remove(WIFI_QUESTIONNAIRE_FIRST_DATE)
            editor.apply()
        }
    }
}
