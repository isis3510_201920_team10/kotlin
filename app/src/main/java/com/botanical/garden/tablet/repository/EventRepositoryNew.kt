package com.botanical.garden.tablet.repository

import android.content.Context
import com.botanical.garden.tablet.model.*
import com.google.common.reflect.TypeToken
import org.json.JSONArray
import org.json.JSONObject
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class EventRepositoryNew {
    suspend fun getAllEvents(context: Context): Array<EventModelNew>
    {
        val allEvents: MutableList<EventModelNew> = mutableListOf<EventModelNew>()
        val jsonString: String = context.assets.open("data/events.json").bufferedReader().use {it.readText()}
        val jsonArray: JSONArray = JSONArray(jsonString)


        return withContext(Dispatchers.IO) {
            for(j in 0 until jsonArray.length()){

                val currentEvent: JSONObject = jsonArray.getJSONObject(j)

                val id = currentEvent.getString("id")
                val name = currentEvent.getString("name")
                val imageUrl = currentEvent.getString("imageUrl")
                val description = currentEvent.getString("description")
                val jsonFeatures = currentEvent.getJSONObject("features")

                val gson = GsonBuilder().create()
                val type = object:TypeToken<Map<String, String>>(){}.type

                val result = gson.fromJson<Map<String, String>>( jsonFeatures.toString(), type)

                val lat = currentEvent.getDouble("latitude")
                val lng = currentEvent.getDouble("longitude")

                val startDate = currentEvent.getString("startDate")
                val endDate = currentEvent.getString("endDate")

                val event = EventModelNew(id,name,imageUrl,description,DetailType.EVENT,result, lat,lng, startDate,endDate)

                allEvents.add(event)

            }

            allEvents.toTypedArray()
        }
            }
}