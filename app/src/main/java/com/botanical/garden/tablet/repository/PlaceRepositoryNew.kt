package com.botanical.garden.tablet.repository

import android.content.Context
import android.util.Log
import com.botanical.garden.tablet.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject

class PlaceRepositoryNew {

    suspend fun getAllPlaces(context: Context): Array<PlaceModelNew>
    {
        val allPlaces: MutableList<PlaceModelNew> = mutableListOf<PlaceModelNew>()
        val jsonString: String = context.assets.open("data/zones.json").bufferedReader().use {it.readText()}
        val jsonArray: JSONArray = JSONArray(jsonString)


        return withContext(Dispatchers.IO) {

            for(j in 0 until jsonArray.length()){

                val chosenZone: JSONArray = jsonArray.getJSONObject(j).getJSONArray("places")

                for(i in 0 until chosenZone.length()){

                    val features:MutableMap<String,String> = mutableMapOf()
                    val currentPlace: JSONObject = chosenZone.getJSONObject(i)
                    Log.v("JSON",chosenZone.toString())


                    val id: String = currentPlace.getString("id")
                    val name: String = currentPlace.getString("name")
                    val descripcion = currentPlace.getString("description")
                    val imageUrl = currentPlace.getString("imageUrl")
                    val lat = currentPlace.getDouble("latitude")
                    val long = currentPlace.getDouble("longitude")


                    val place = PlaceModelNew(id,name,imageUrl,descripcion,
                        DetailType.PLACE,features,lat,long)
                    allPlaces.add(place)

                }


            }
            allPlaces.toTypedArray()
        }
    }
}