package com.botanical.garden.tablet.ui.explore

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.botanical.garden.tablet.model.PlantModelNew
import com.botanical.garden.tablet.model.ZoneSimpleModel
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng



@BindingAdapter("data")
fun setCloseItems(view: RecyclerView,data:LiveData<List<PlantModelNew>>) {
    if(data.value==null)
        return

    data.value?.let{
        val adapter=view.adapter as FlowerAdapter
        adapter.setData(it.toTypedArray())
    }
}

@BindingAdapter("endLatLng","currentZone",requireAll = true)
fun setZoneName(view: TextView, endLatLng: LiveData<Pair<LatLng?,LatLng?>>?, zone:LiveData<ZoneSimpleModel>){
    if(endLatLng?.value==null)
        return
    var zoneName = "Zona no reconocida"
    zone.value?.let{
        zoneName = it.name
    }
    view.text = zoneName
}

@BindingAdapter("endLatLng","currentZone",requireAll = true)
fun setZoneImage(view: ImageView, endLatLng: LiveData<Pair<LatLng?,LatLng?>>?, zone:LiveData<ZoneSimpleModel>){
    if(endLatLng?.value==null)
        return
    zone.value?.let{
        Glide.with(view.context).load(it.imageUrl).into(view)
    }
}