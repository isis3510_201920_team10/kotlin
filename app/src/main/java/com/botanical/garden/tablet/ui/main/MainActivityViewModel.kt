package com.botanical.garden.tablet.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val _isConnected=MutableLiveData<Boolean>(true)

    val isConnected: LiveData<Boolean>
        get() = _isConnected

    fun setIsConnected(isConnected:Boolean){
        _isConnected.value=isConnected
    }
}