package com.botanical.garden.tablet.ui.explore

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.botanical.garden.tablet.R
import com.botanical.garden.tablet.databinding.FragmentExploreFragmentJBinding
import com.botanical.garden.tablet.model.PlantModelNew
import com.botanical.garden.tablet.ui.main.MainActivity
import com.botanical.garden.tablet.ui.main.MapViewModel


import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_explore_fragment_j.view.*



class ExploreFragmentJ : Fragment(), FlowerAdapter.PlantClickListener {
    private var adapter: FlowerAdapter? = null
    private var mListener: OnFragmentInteractionListener? = null

    private var zoneText: TextView? = null
    private var recyclerView: RecyclerView? = null


    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentExploreFragmentJBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_explore_fragment_j, container, false
        )
        val v = binding.root

        recyclerView = v.flowers_rv
        recyclerView?.layoutManager = GridLayoutManager(context, 2)
        adapter = context?.let { FlowerAdapter(it, this) }
        recyclerView!!.adapter = adapter

        zoneText = v.zone_name
        Log.v("Zone text", zoneText!!.toString())

        activity?.let {
            binding.viewModel = ViewModelProviders.of(it).get(MapViewModel::class.java) // Get viewmodel from activity
        }

        binding.lifecycleOwner=viewLifecycleOwner

        // Firebase analytics event
        if(context==null)
            return v

        val fab:FloatingActionButton = v.float_btn
        fab.setOnClickListener{
            (activity as MainActivity).goToMap(it,null,null)
        }
        // Inflate the layout for this fragment
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onItemClick(view: View, position: Int) {
        val bundle = bundleOf("plant" to adapter!!.getItem(position))
        findNavController().navigate(R.id.navigation_plant_detail,bundle)
   }


    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(plant: PlantModelNew)
    }

    override fun onDestroyView() {
        recyclerView?.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                // no-op
            }

            override fun onViewDetachedFromWindow(v: View) {
                recyclerView?.adapter = null
                recyclerView=null
                zoneText=null
                adapter=null
                mListener=null
            }
        })
        super.onDestroyView()
    }
    override fun onDestroy() {
        zoneText = null
        recyclerView?.adapter=null
        recyclerView = null
        adapter=null
        mListener=null
        super.onDestroy()
    }
}// Required empty public constructor
