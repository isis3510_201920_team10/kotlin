package com.botanical.garden.tablet.model

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventModelNew(
    override var id: String="",

    override val name:String,

    override val imageUrl: String,

    override val description: String,

    override val type:DetailType=DetailType.EVENT,

    override val features: Map<String,String>,

    override val latitude: Double,

    override val longitude: Double,

    private val startDate:String?,

    private val endDate:String?
): Parcelable,ItemModel() {
    override fun toDetailModel(): LiveData<DetailModelNew> {
        val ans= MutableLiveData<DetailModelNew>()

        val detail=DetailModelNew(
            id,
            imageUrl,
            name,
            type,
            description,
            features,
            latitude,
            longitude,
            null,
            startDate,
            endDate
        )

        ans.postValue(detail)
        return ans
    }
}