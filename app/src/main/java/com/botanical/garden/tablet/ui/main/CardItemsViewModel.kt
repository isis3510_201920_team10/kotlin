package com.botanical.garden.tablet.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class CardItemsViewModel(application: Application) : AndroidViewModel(application) {

    private val _id= MutableLiveData<Int?>(null)
    private val _details= MutableLiveData<String>("")

    val id: LiveData<Int?>
        get() = _id

    val details: LiveData<String>
        get() = _details

    fun setId(id:Int?){
        _id.postValue(id)
    }


}